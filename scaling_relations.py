import numpy as np


# =====================================================================================
#
# The support routines
#

def scaling_relation (A, B, C, zed, m200) :
    scale = A * m200**B * ((1+zed)/1.0)**C
    return scale
def pressure_from_mass (Ap, Bp, Cp, zed, m200) :
    pressure = scaling_relation(Ap, Bp, Cp, zed, m200)
    return pressure
def xc_from_mass (Ax, Bx, Cx, zed, m200) :
    xc = scaling_relation(Ax, Bx, Cx, zed, m200)
    return xc
def beta_from_mass (Abeta, Bbeta, Cbeta, zed, m200) :
    beta = scaling_relation(Abeta, Bbeta, Cbeta, zed, m200)
    return beta

# This is not the Saro form, which uses M500 and H_z. We make it consistent
# withe the other scaling relations
def lambda_from_mass (Alambda, Blambda, Clambda, zed, m200) :
    lambdas = scaling_relation(Alambda, Blambda, Clambda, zed, m200)
    return lambdas

# and we will use Mechior et al 2016 eq 51 and table 4
# mass_pivot = 2.35 e14
# fl = 1.12
# gz = 0.18
def mass_from_lambda (mass_pivot, fl, gz, zed, lambdas) :
    mass = mass_pivot * (lambdas/30.)**fl * ( (1+zed)/1.5)**gz
    return mass

def r200_from_mass (hz,m200) :
    # m200 is in units of 10^14 solar masses
    # G needs to be in units of solar mass
    #G = 6.67259e-8  ;# cm^3 gm^-1 s^-2
    #G = G * (1e-5)**3 ;# (km/cm)^3
    #G = G * 1.99e33  ;# gm/M_sun    to reach G in units of km^3/s^2/M_sun = 1.328e11
    G = 1.3278e+11 #gm/M_sun
    
    # r200 = (G/100) * m200^1/3 / H(z)^2/3  (equation 13)
    # h will be in km/s/Mpc
    # if you work through the units, there is one km/Mpc conversion left
    # going from G M200/H^2  3.086e+19 ;# km/Mpc
    r200 = ((G/100.) * m200 * 1e14 * hz**(-2.) * (3.086e19)**-1 )**(1./3.) 
    return  r200


# Y is the helium fraction. See Vikram, Lidz, Jain, the dicussion after eq 6
def f_from_Y(Y=0.24) :
    f = (4.-2*Y)/(8.-5*Y)
    return f

# equation 28b in my Notes
# pressure has units of kg /m/s^2       
def p200_from_mass(hz,m200,omega_b,omega_m) :
    # m200 is in units of 10^14 solar masses
    # hz in km/s/Mpc

    # G in km^3/s^2/solar mass: G = 1.328e11
    # (M^2/G)**(1./3.) = 1.7833e11  m_sun^2/km^2/s^2
    # converting H(z) from km/s/Mpc to 1/s: 3.086e19 km/Mpc
    # km_per_mpc = 3.086e19
    # a = (3./8./np.pi) * (1./2.)**(1./3)*200**(4/3.) # = 25.264
    b = omega_b/omega_m                  # = 0.164
    c = hz**(8./3.)                      # = 9.30e4 for Ho=72
    # (M^2/G)**(1./3.) = 4.222e5   M_sun s^2/km
    # d =  4.222e5 * km_per_mpc    # 1.303e25  M_sun s^2/mpc
    # e = km_per_mpc**(-8./3.)  # convert km/s/mpc to 1/s
    #p200 = 0.458 * m200**(2./3.) * hz**(8./3.) * omega_b/omega_m
    #a * d * e / 2 = 7.7e-26
    p200 = m200**(2./3.)* b * c * 7.7e-26 ;# 4.07e-22/2.
    return p200

# eq 6 in Vikram, Lidz, Jain
# used to double check the above. It works.
def p200_from_mass2(hz, m200, omega_b, omega_m) :
    r200 = r200_from_mass(hz, m200)
    rho_c = rho_crit(hz)
    #print "r200", r200/1.1
    #raise Exception("here")
    # m200 is in units of 10^14 solar masses
    # hz in km/s/Mpc
    # G needs to be in units of solar mass
    # G = 6.67259e-8  ;# cm^3 gm^-1 s^-2
    # G = G * (1e-5)**3 ;# (km/cm)^3
    # G = G * 1.99e33  ;# gm/M_sun   to reach G in units of km^3/s^2/M_sun = 1.328e11 
    # G = G * (1./3.086e19)**3 ;# (Mpc/km)**3    to reach G in units of Mpc^3/s^2/M_sun = 4.519e-48
    G = 4.519e-48 #Mpc^3/s^2/M_sun 
    m200 = m200*1e14  ;# to get into M_sun units
    # M_sun km^2 /Mpc^3/s^2
    p200 = 200 * rho_c * (omega_b/omega_m ) * G * m200/(2.*r200)
    # Mpc^3/s^2/M_sun * M_sun/Mpc^3 M_sun Mpc^-1
    # s^-2  M_sun Mpc-1 = M_sun/Mpc/s^2
    #print "compare p200, p200_2,", p200, p200_from_mass2(hz,m200/1e14,omega_b,omega_m)
    return p200

# support for the above test routine, otherwise not used
def rho_crit (hz, G=1.328e11) :
    # hz in km/s/Mpc
    # G in units of km^3/s^2/M_sun, need one more conversion
    G = G * (1./3.086e19) # G in units of km^2 Mpc/s^2/M_sun
    rho = 3*hz**2 /8/ np.pi/ G
    # rho in units of M_sun/Mpc^3
    return rho

