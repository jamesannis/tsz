import numpy as np

import pylab as pl

from scipy.signal import convolve, convolve2d



sigma = 0.5#0.1

def f2d(x,y, sigma):
 
    #ff=(x**2+y**2)
    #return ff
    
    ff= np.exp(-(x**2. + y**2)/2./sigma**2)
    return ff 

def f1d(x, sigma):

    ff= np.exp(-x**2/2./sigma**2)
    #ff=(x**2)
    return ff



def g2d(x, y, sigma):
    ff=(1./2./np.pi/sigma**2) * np.exp(-(x**2. + y**2)/2./sigma**2)
    ff=ff/np.sum(ff)
    return ff



def g1d(x, sigma):

    ff= (1./sigma/np.sqrt(2.*np.pi)) * np.exp(-x**2/2./sigma**2)
    ff=ff/np.sum(ff)
    return ff


dx=10/202.0
x = np.arange(-5, 5+dx, dx)

xx,yy = np.meshgrid(x,x)


xg = np.arange(-10.0*sigma, 10.0*sigma, dx)
#xg=x
xxg,yyg = np.meshgrid(xg,xg)

g2 = g2d(xxg, yyg, sigma)

g1 = g1d(xg, sigma)


#f2 = f2d(xx,yy)

f2 = f2d(xx,yy, sigma*2)

f2c = convolve2d(f2, g2, mode='same')



#f1 = f1d(x)

f1 = f1d(x, sigma*2)

f1c = convolve(f1, g1, mode='same')

#A = f2[100][100]/f1[100]



print len(x)
print xx[101]
print yy[101]
print np.shape(f2c)
print np.shape(f2)
print len(f1c)
print len(f1)
'''
pl.plot(x, f2c[101], lw=3., c='k', label='2D Convolution')
pl.plot(x, f2[101], lw=3., c='b', label='Original Profile')
#pl.plot(x, (f2[100]+sigma**2), lw=3., c='r', label='analytical')
pl.plot(x, f1c, c='r',label='1D Convolution')

print np.sum(f2), np.sum(f2c)
print np.sum(f1), np.sum(f1c)
pl.legend(loc=4)
pl.xlabel(r'$r$', fontsize=18)
pl.ylabel(r'Profile', fontsize=18)
#pl.yscale('log')
pl.savefig('test2_2.png')
'''
pl.plot(x, f1c/f2c[101])
pl.xlabel(r'$r$', fontsize=15)
pl.ylabel(r'Ratio (1D/2D)', fontsize=15)
pl.savefig('test2_2_ratio.png')

