import os
import numpy as np
import hankel
import two_halo
from scipy.interpolate import interp1d
from scipy.interpolate import RectBivariateSpline
import matplotlib.pyplot as plt

def do(z1,z2,plot=True, bias=-999, vhm=True):
    import configparser

    cosmosis_dir = "output_cross/"
    config = configparser.ConfigParser()
    config.read('y_cross_rm_values.ini')
    cosmo = "cosmological_parameters"
    h0 = config.getfloat(cosmo,"h0")
    omega_b = config.getfloat(cosmo,"omega_b")
    omega_m = config.getfloat(cosmo,"omega_m")
    psf_fwhm = config.getfloat(cosmo,"PSF")
    zed1 = z1
    zed2 =  z2
    zed = (zed1+zed2)/2.

    #==================================
    #
    # cosmological parameters
    #
    #==================================

    # matter-matter power spectrum
    pk_z_table = np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/z.txt"), skip_header=1)
    k_h = np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/k_h.txt"), skip_header=1)
    p_k_table=np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/p_k.txt"), skip_header=1).reshape([np.size(pk_z_table),np.size(k_h)]).T
    p_k = RectBivariateSpline(k_h,pk_z_table,p_k_table)

    z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
    da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
    hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5 

    # tinker mass function  in critical; to get background mult by omega_m
    mz_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/z.txt"), skip_header=1)
    m_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/m_h.txt"), skip_header=1)
    dndlnMh_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/dndlnmh.txt"), skip_header=1).reshape([np.size(mz_table),np.size(m_table)]).T

    # tinker bias
    tinker_bias_ln_mass = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/ln_mass.txt"), skip_header=1)
    tinker_bias_bias = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/bias.txt"), skip_header=1)
    bias_interpolator_2d = RectBivariateSpline(\ 
         tinker_bias_zed, tinker_bias_ln_mass, tinker_bias_bias)
    bias_at_z = bias_interpolator_2d(zed, tinker_bias_ln_mass).flattein
    poly_coeff = np.polyfit( tinker_bias_ln_mass, np.log(bias_at_z), 3)

    #==================================
    #
    # one-halo results
    #
    #=================================
    szb = "tSZ"
    phys_r_samples = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/phys_radius_two_sum.txt"), skip_header=1)
    h2_mass_weighted_profile = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/profile_two_sum.txt"), skip_header=1)
    h1_mass_weighted_profile = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/profile_one.txt"), skip_header=1)
    #  bias in a lambda bin to multiply the matter-matter power spectrum by
    
    if bias == -999 :
        keyword = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/values.txt"), unpack=True,usecols=0,dtype="str")
        values = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/values.txt"), unpack=True,usecols=2)
        ix = keyword == "b_mu_bin"
        b_mu_bin = values[ix]
    else :
        b_mu_bin = bias
    #==================================
    #
    # main computation
    #
    #=================================

    if not vhm :
        r_samples, corr, krange, p_k_sz, phys_r_samples,  \
            h1_mass_weighted_profile, h2_mass_weighted_profile,  \
            p_k_matter, p_k_1h, p_k_2h, mmcorr, h1corr, h2corr = \
            two_halo.main( zed1, zed2, phys_r_samples, 
                h1_mass_weighted_profile, h2_mass_weighted_profile, 
                b_mu_bin, k_h, pk_z_table, p_k_table, testing=True)
    
        if plot :
            plt.clf(); 
            ax=plt.subplot(1,1,1);ax.set_yscale("log"); ax.set_xscale("log"); 
            plt.plot(r_samples,corr);
            plt.plot(r_samples,h1corr,c="r");
            plt.plot(r_samples,h2corr,c="g")
            plt.scatter(phys_r_samples, h1_mass_weighted_profile,c="yellow")
            plt.xlabel("radius (log(Mpc))")
            plt.ylabel("log(Y)")
     
        #return r_samples, corr, krange, p_k_sz, phys_r_samples,  \
        #    h1_mass_weighted_profile, h2_mass_weighted_profile,  \
        #    p_k_matter, p_k_1h, p_k_2h, mmcorr, h1corr, h2corr 
        return r_samples, corr, phys_r_samples,  \
            h1_mass_weighted_profile, h1corr, h2corr 
    else :
        r_samples, h2corr, h2corr_sm = \
            two_halo.main2( zed1, zed2, b_mu_bin, k_h, pk_z_table, p_k_table, \
            dndlnMh_table, m_table, mz_table, z_table, hz_table, 
            tinker_bias_ln_mass, tinker_bias_bias ,
            h0, omega_b, omega_m, psf_fwhm)

        if plot :
            plt.clf(); 
            ax=plt.subplot(1,1,1);ax.set_yscale("log"); ax.set_xscale("log"); 
            #plt.plot(r_samples,h1corr,c="r");
            plt.plot(r_samples,h2corr,c="g")
            plt.plot(r_samples,h2corr_sm,c="b")
            plt.xlabel("radius (log(Mpc))")
            plt.ylabel("log(Y)")
     
    
        return r_samples, h2corr, h2corr_sm

    
# reload(th);reload(oh); pr,h1,h2,b,ot,op= oh.do(doPlot=False);r,c,k,pk,pr,h1,h2,pkm,pk1,pk2,mmc,h1c,h2c=th.do(doPlot=False); t1,tp1,t2,tp2,t2m,tp2m,yr,yp=th.y(r,c,h1c,h2c,pr,h1)

def y(z1,z2,r_samples, corr, h1corr, h2corr, phys_r_samples, h1mwp, data="sdss.txt") :
    import sz_profile
    import configparser

    cosmosis_dir = "output_cross/"
    z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
    da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
    hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5 
    config = configparser.ConfigParser()
    config.read('y_cross_rm_values.ini')
    cosmo = "cosmological_parameters"
    psf_fwhm   = config.getfloat(cosmo, "PSF")
    zed1 = z1
    zed2 =  z2
    zed = (zed1+zed2)/2.

    ix = r_samples <= 20.
    one_halo_radius = r_samples[ix]
    two_halo_radius = r_samples[ix]
    one_halo_y = h1corr[ix]
    two_halo_y = h2corr[ix]
    two_halo_ym = corr[ix]

    # the corr function and rad profile have slightly different central amplitudes
    # try replacing corr with h1mpw to see  (phys_r_samples, h1mpw)
    one_halo_y = interp1d(phys_r_samples, h1mwp, \
        bounds_error=False, fill_value="extrapolate")(r_samples[ix])
    #one_halo_y = interp1d(r_samples, corr, \
    #    bounds_error=False, fill_value="extrapolate")(r_samples[ix])

    one_halo = interp1d(one_halo_radius, one_halo_y, bounds_error=False, fill_value="extrapolate")
    two_halo = interp1d(two_halo_radius, two_halo_y, bounds_error=False, fill_value="extrapolate")
    two_halo_m = interp1d(two_halo_radius, two_halo_ym, bounds_error=False, fill_value="extrapolate")

    #==================================
    #
    # main computation
    #
    #==================================
    
    m200 = 1 ;# this won't be used in the project_and_convolve_with_beam
    sz = sz_profile.szProfile(zed,m200,z_table, da_table, hz_table)

    halo = one_halo(one_halo_radius) 
    ix = halo <= 0
    halo[ix] = halo[np.invert(ix)].min()
    one_theta, one_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 

    halo = two_halo(one_halo_radius) 
    two_theta, two_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 

    halo = two_halo_m(one_halo_radius) 
    two_theta_m, two_profile_m = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 

    halo = one_halo(one_halo_radius) + two_halo_m(one_halo_radius)
    y_theta, y_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 

    plt.clf(); ax=plt.subplot(1,1,1);
    # sdss.txt
    ax.set_yscale("log"); plt.ylim(1e-8,1e-5); plt.xlim(1e-1,100)
    # sim.txt
    #ax.set_yscale("log"); plt.ylim(1e-8,1e-4); plt.xlim(1e-1,100)
#    ax.set_yscale("linear"); plt.ylim(1e-10,.15e-5)
    ax.set_xscale("log"); 
    plt.plot(y_theta,y_profile,c="red");
    plt.plot(one_theta,one_profile,c="orange")
    plt.plot(two_theta,two_profile,c="green")
    plt.plot(two_theta_m,two_profile_m,c="blue")
    meanr,meany,meanyerr = np.genfromtxt(data, unpack=True, skip_header=1)
    ix = meanr < 21
    plt.scatter(meanr[ix],meany[ix],c="cyan");
    plt.errorbar(meanr[ix],meany[ix], yerr=meanyerr[ix],c="cyan");
    plt.xlabel("log radius (arcmin)");plt.ylabel("log Y");
    #meanr,meany,meanyerr = np.genfromtxt("sim.txt", unpack=True, skip_header=1)
    #ix = meanr < 50
    #plt.scatter(meanr[ix],meany[ix])
    #plt.errorbar(meanr[ix],meany[ix], yerr=meanyerr[ix])
    print one_profile[0:10].mean(), two_profile[0:10].mean(), two_profile_m[0:10].mean(), y_profile[0:10].mean()

    return one_theta, one_profile, two_theta, two_profile, \
        two_theta_m, two_profile_m, y_theta, y_profile



