from cosmosis.datablock import option_section, names
import numpy as np
from scipy.interpolate import interp1d

def setup(options):
     file = "SDSS_rdmp_zge0.200000_lt0.250000_lmdge30_lt40.txt"
     data_radius, data_y, data_err = np.genfromtxt(file, unpack=True, skip_header=1)
     file = "cov.in"
     cov = np.genfromtxt(file,unpack=True)
     return data_radius,data_y,data_err, cov

def execute(block, config):
    likes = names.likelihoods
  # data
    data_radius,data_y,data_err,cov = config
 # theory   
    szb = "tSZ"
    r = block[szb,'y_theta'] 
    y = block[szb,'y_profile'] 
    last_point = r[-1]
    first_point = r[0]
    ix = (data_radius > first_point) & (data_radius < last_point)
    data_radius = data_radius[ix]
    data_y = data_y[ix]
    data_err = data_err[ix]
    ix = np.nonzero(np.invert(ix))[0]
    cov = np.delete(cov, ix, 0) ;# rows
    cov = np.delete(cov, ix, 1) ;# columns
    inv_cov = np.linalg.inv(cov)
        
    ytheory = interp1d(r,y)
    # does the theory go out to the data radius?
    #print r.min(), r.max()
    #print data_radius.min(), data_radius.max()

    d = data_y - ytheory(data_radius)
    chi2 = np.einsum('i,ij,j', d, inv_cov, d)
    ylike = -0.5*chi2

    #print "sqrt diagonal of covariance matrix"
    #for i in range(data_y.size) :
        #print np.sqrt(cov[i,i]),
    #print "" 
    #print "errors"
    #print data_err
    block[likes, 'YLIKE_LIKE'] = ylike
    return 0
    
    
