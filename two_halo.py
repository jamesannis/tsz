import numpy as np
import hankel
import scaling_relations
from numba import jit
from scipy.interpolate import interp1d
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import RectBivariateSpline

def main( zed1, zed2, phys_r_samples, 
    h1_mass_weighted_profile, h2_mass_weighted_profile, b_mu_bin,
    k_h, z_table, p_k_table, testing=False) :

    p_k = RectBivariateSpline(k_h,z_table,p_k_table)
    zed = (zed1+zed2)/2.

    #==================================
    #
    # main computation
    #
    #=================================
    # we assume k = 2 pi/lambda , and that r = lambda, thus r= 2 pi/k and k = 2pi/r
    # this range works for matter matter power spectrum
    krange = np.logspace(np.log10(1e-3),np.log10(1.),500)
    p_k_matter = p_k(krange,zed).flatten() 

    # this works fine for mmPS and for full 2-halo term
    k_samples = np.logspace(np.log10(0.05),np.log10(25),1000)
    # limiting the k_samples to near k=1 makes the FFT 
    # of the 2h profile (no mmPs) more # or less work. 
    # It also works fine  for mmPS and for full 2-halo term
    k_samples = np.logspace(np.log10(0.001),np.log10(10),100)

    # works for both matter power spectra and for 2-halo term, shows BAO bump
    r_samples = np.linspace(0.05,150.,1000)

    # work up the spherical bessel transform, the real version of a hankel
    h = hankel.SphericalHankelTransform(nu=0,N=1000,h=0.001)

    sz_ps = []
    for k in k_samples :
        # do a polynomial fit to the integrand to enable extrapolation
        x = k*phys_r_samples
        # do the (oscillating, horrible, hard!) integral
        f = interp1d(x, h2_mass_weighted_profile* x**2, bounds_error=False, fill_value="extrapolate")
        sz = h.transform(f)[0] * (4 * np.pi)/k**3
        sz_ps.append( sz )
    sz_ps = np.array(sz_ps)
    sz_ps = interp1d(k_samples, sz_ps, bounds_error=False, fill_value="extrapolate")

    p_k_sz = p_k_matter * sz_ps(krange)

    corr = []
    for r in r_samples :
        x= r * krange
        p_k_sz_i = interp1d(x, p_k_sz*x**2 , bounds_error=False, fill_value="extrapolate")
        c = h.transform(p_k_sz_i)[0] /(2 * np.pi**2 * r**3)
        corr.append(c)
    corr = np.array(corr)

    # playing with a desparate hack: normalize to halo_2
    interp1 = interp1d(phys_r_samples , h2_mass_weighted_profile)
    interp2 = interp1d(r_samples , corr)
    #corr = corr *  interp1(0.5)/interp2(0.5) 

    corr = b_mu_bin * corr
    
    if not testing :
        return r_samples, corr, krange, p_k_sz

    else :
        # works for BAO and for SZ
        h = hankel.SphericalHankelTransform(nu=0,N=1000,h=0.005)
        p_k_mm = p_k_matter
        mmcorr = []
        for r in r_samples :
            x= r * krange
            p_k_mm_i = interp1d(x, p_k_mm*x**2 , 
                bounds_error=False, fill_value="extrapolate")
            c = h.transform(p_k_mm_i)[0] /(2 * np.pi**2 * r**3)
            mmcorr.append(c)
        mmcorr = np.array(mmcorr)

        # the power spectrum of the SZ seems to be close to
        # a power law in k, log(ps)
        # let's model this as
        model1h = np.array([-0.30 -5.1])
        model2h = np.array([-0.35 -7.5])
        model1h = np.array([-0.29624893 ,  -5.1434586 ]) # order = 1, i.e. linear fit
        model1h = np.array([-0.29624893, -5.1434586 ]) # same but at k<= 30
        model1h = np.array([-0.26264589, -5.16070851]) # 0.01 < k < 30
        # seems like all the information is inside that range, for planck. 
        # oscillatory wiggles leftover
        model2h = np.array([-0.35126501 , -7.48249432])
        ps_1_model = np.poly1d(model1h)
        ps_2_model = np.poly1d(model2h)

        # works well for the SZ profile
        h = hankel.SphericalHankelTransform(nu=0,N=1000,h=0.005)
        sz1h_ft = []
        counter = 0
        for k in k_samples :
            x = k*phys_r_samples
            f = interp1d(x, h1_mass_weighted_profile* x**2, 
                bounds_error=False, fill_value="extrapolate")
            sz = h.transform(f)[0] * (4 * np.pi)/k**3
            sz1h_ft.append( sz )
            counter+= 1
        sz1h_ft = np.array(sz1h_ft)
        sz1h_ft = interp1d(k_samples, sz1h_ft, 
            bounds_error=False, fill_value="extrapolate")
        p_k_1h = sz1h_ft(krange)
    
        #p_k_1h = 10**ps_1_model(krange)
        h1corr = []
        for r in r_samples :
            x= r * krange
            p_k_1h_i = interp1d(x, p_k_1h*x**2 , 
                bounds_error=False, fill_value="extrapolate")
            c = h.transform(p_k_1h_i)[0] /(2 * np.pi**2 * r**3)
            h1corr.append(c)
        h1corr = np.abs(np.array(h1corr))

        sz2h_ft = []
        counter = 0
        for k in k_samples :
            x = k*phys_r_samples
            f = interp1d(x, h2_mass_weighted_profile* x**2, 
                bounds_error=False, fill_value="extrapolate")
            sz = h.transform(f)[0] * (4 * np.pi)/k**3
            sz2h_ft.append( sz )
            counter+= 1
        sz2h_ft = np.array(sz2h_ft)
        sz2h_ft = interp1d(k_samples, sz2h_ft, 
                bounds_error=False, fill_value="extrapolate")
        p_k_2h = sz2h_ft(krange)
        #p_k_2h = 10**ps_1_model(krange)
        h2corr = []
        for r in r_samples :
            x= r * krange
            p_k_2h_i = interp1d(x, p_k_2h*x**2 , 
                bounds_error=False, fill_value="extrapolate")
            c = h.transform(p_k_2h_i)[0] /(2 * np.pi**2 * r**3)
            h2corr.append(c)
        h2corr = np.abs(np.array(h2corr))
    
    return r_samples, corr, krange, p_k_sz, \
        phys_r_samples, h1_mass_weighted_profile, \
        h2_mass_weighted_profile, p_k_matter, \
        p_k_1h, p_k_2h, mmcorr, h1corr, h2corr


def main2 ( zed1, zed2, b_mu_bin,
            k_h, pz_table, p_k_table, 
            dndlnMh_table, m_table, mz_table,
            z_table, hz_table,
            tinker_bias_zed, tinker_bias_ln_mass, tinker_bias_bias,
            h0, omega_b, omega_m, testing=False  ) :
    '''
        Eq. 8 in Vikram, Lidz & Jain
        pk_arr: linear power spectrum Mpc^3, 

    Eq. 8 in Vikram, Lidz & Jain
    pk_arr: linear power spectrum Mpc^3, marr, karr: wavelength Mpc^-1, bmf Mpc^-3, dlnk, dlnm, hb: bias of mass, z, BryanDelta: omega_m(z), rho_critical, omega_b0, omega_m0, cosmo_h, smooth: PSF smoothing scale in arcmin)
    '''
    if h0 > 2: raise Exception("no, h/100")
    zed = (zed1+zed2)/2.

    # shall we assume the absolute isn't needed?
    #area = 4.0*np.pi*fraction_of_sky  # fraction of the sky covered, taking mass function as per sterdadian
    area = 1.0
    dndlnMh_table = dndlnMh_table * area  # numbers inside survey area
    #2D interpolator into mass function; mass in linear in solar masses
    mass_function = RectBivariateSpline(np.log10(m_table),mz_table,dndlnMh_table)
    
    p_k = RectBivariateSpline(k_h,pz_table,p_k_table)
    bias_interpolator_2d = RectBivariateSpline(
         tinker_bias_zed, tinker_bias_ln_mass, tinker_bias_bias)
    bias = bias_interpolator_2d(zed,tinker_bias_ln_mass)
    bias_interpolator = interp1d(tinker_bias_ln_mass, bias,  \
        bounds_error=False, fill_value="extrapolate")

    z_table = z_table[::-1]
    hz_table = hz_table[::-1]
    hz_interpolator = interp1d(z_table, hz_table)
    hz = hz_interpolator(zed)

    pk_arr = p_k(k_h,zed).flatten()
    mf_arr = mass_function(np.log10(m_table),zed).flatten()
    bmf = mf_arr * bias_interpolator(np.log(m_table)).flatten()
    bryanDelta = BryanDelta(omega_m) ;# should be omega_m(z) but what is that?
    rho_c=2.775e11 # h^2 M_solar Mpc^-3.
    r200_arr = scaling_relations.r200_from_mass (hz,m_table) 

    # works for both matter power spectra and for 2-halo term, shows BAO bump
    r_samples = np.linspace(0.05,30.,10)
    # JTA hack for speedup for debugging
    #r_samples = np.linspace(0.5,2.,3)
    dlnm = np.log(m_table[1]/m_table[0])
    dlnk = np.log(k_h[1]/k_h[0])
    cosmo_h = h0
    
    xi,xism = np.array([]),np.array([])
    for r in r_samples :
        #print np.shape(m_table),np.shape(bmf)
        #print m_table[800], bmf[800], mf_arr[800]
        #raise Exception("jack")
        xi_at_r = integrate_2halo( r, pk_arr, m_table, r200_arr, k_h, \
            bmf, dlnk, dlnm, b_mu_bin, zed, bryanDelta,  rho_c, \
            omega_b, omega_m,  cosmo_h)
        xi = np.append(xi, xi_at_r)

    return r_samples, xi
     


#@jit((nb.float64)(nb.float64, nb.float64[:], nb.float64[:], nb.float64[:], nb.float64[:], nb.float64, nb.float64, nb.float64, nb.float64, nb.float64, nb.float64, nb.float64, nb.float64, nb.float64, nb.float64),nopython=True)
@jit(nopython=True)
def integrate_2halo(r, pk_arr, marr, r200arr, karr, bmf, dlnk, dlnm, hb, z, BryanDelta, rho_critical, omega_b0, omega_m0, cosmo_h): 
    '''
    Eq. 8 in Vikram, Lidz & Jain
    pk_arr: linear power spectrum Mpc^3, marr, karr: wavelength Mpc^-1, bmf Mpc^-3, dlnk, dlnm, hb: bias of mass, z, BryanDelta: omega_m(z), rho_critical, omega_b0, omega_m0, cosmo_h)
    '''    
    rparr = np.linspace(0.1, 6., 100)
    drp = rparr[1] - rparr[0]
    intk = 0.0
    intksm = 0.0
    ki = 0
    #print 1, dr
    for ki, pk in enumerate(pk_arr):
        k = karr[ki]
        intm = 0.0
        mi = 0
        for mi, m in enumerate(marr):
            up = 0.0
            r200 = r200arr[mi]
            for rp in rparr:
                up += (4*np.pi*rp*np.sin(rp*k) * drp * battaglia_profile(rp, m, r200, z, BryanDelta, rho_critical, omega_b0, omega_m0, cosmo_h) / k)
                #print rp, up
                #print battaglia_profile,rp, m, z, BryanDelta, rho_critical, omega_b0, omega_m0, cosmo_h
                #print rp, m, r200, z, BryanDelta, rho_critical, omega_b0, omega_m0, cosmo_h
            intm += (bmf[mi] * up * dlnm)
            #intm += (bmf[mi] * dlnm)
        #print m, bmf[mi] , up, dlnm
        #print dlnk , k , r , pk , hb , intm 
        intk += (dlnk * k * k * np.sin(k * r) * pk * hb * intm / r)
    #print r, intk / 2. / np.pi
    xi = intk / 2. / np.pi / np.pi
    #xism = intksm / 2. / np.pi / np.pi
    #return xi, xism
    return xi
 

@jit( nopython=True)
def battaglia_profile(r, M200, R200, z, BryanDelta, rho_critical, omega_b0, omega_m0, cosmo_h):
    '''
    Using Battaglia et al (2012). 
    Eq. 10. M200 in solar mass and R200 in Mpc
    Retrun: 
        Pressure profile in keV/cm^3 at radius r
    '''
    #It seems R200 is in the physical distance, i.e. proper distance
    #Need to multiplied by (1+z) to get the comoving unit as I am giving r in
    #comoving unit.
    R200 *= (1. + z) #Comoving radius 
    x = r / R200
    #print Mvir, M200, R200
    msolar = 1.9889e30 #kg
    mpc2cm = 3.0856e24 #cm 
    G = 4.3e-9 #Mpc Mo^-1 (km/s)^2 
    alpha = 1.0
    gamma = -0.3
    P200 = 200. * rho_critical * omega_b0 * G * M200 / omega_m0 / 2. / (R200 / (1. + z)) #Msun km^2 / Mpc^3 / s^2

    P0 = 18.1 * ((M200 / 1e14)**0.154 * (1. + z)**-0.758)
    xc = 0.497 * ((M200 / 1e14)**-0.00865 * (1. + z)**0.731)
    beta = 4.35 * ((M200 / 1e14)**0.0393 * (1. + z)**0.415) 
    #print P0, xc, beta
    #print (P200*msolar * 6.24e18 * 1e3 / mpc2cm**3), P0, xc, beta
    pth = P200 * P0 * (x / xc)**gamma * (1. + (x/xc))**(-1. * beta) #(km/s)^2 M_sun / Mpc^3

    #Joule = kg m^2 / s^2, Joule = 6.24e18 eV = 6.24e15 keV
    pth *= (msolar * 6.24e15 * 1e6 / mpc2cm**3) #keV/cm^3. 1e6 implies that I have converted km to m
    p_e = pth * 0.518 #For Y=0.24, Vikram, Lidz & Jain
    return p_e

def BryanDelta(omega_m):
    '''
    Delta_c which include virial mass from Bryan & Norman 1998  
    '''
    x = omega_m - 1.
    return 18 * np.pi * np.pi + 82. * x - 39. * x * x


