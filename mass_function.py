import numpy as np
import os
from scipy.interpolate import UnivariateSpline, RectBivariateSpline,SmoothBivariateSpline
from scipy.interpolate import interp1d

class setup(object):
    """Mass function

    mf = mass_function.setup()
    counts = mf.cluster_counts(zed, m200, del_zed, dm)
"""
    def __init__(self, 
        fraction_sky=0.118,
        Ho=70., om = 0.3, ol = 0.7, ok=0.0
        ):
        """Create an object that will be used to evaluate a mass function

        m200 is in units of 10^14 M_sun
        """
        self.Ho = Ho
        self.ol = ol
        self.om = om
        self.ok = ok
        self.fraction_sky = fraction_sky

        data_dir = "./cosmosis_out/"
        MF_dir = data_dir
        DISTANCES_dir = data_dir
        #
        # read in cosmological parameters as fn(z) from previous run of cosmosis
        #   many of these will be replaced with direct calls in execute
        z_array=np.genfromtxt(MF_dir+"z.txt", unpack=True,comments="#") ;# starts at hi-z
        a_array=np.genfromtxt(MF_dir+"a.txt", unpack=True,comments="#") ;# starts at hi-z
        r_array=np.genfromtxt(MF_dir+"r_h.txt", unpack=True,comments="#")
        dndm_array=np.genfromtxt(MF_dir+"dndlnmh.txt", comments="#")
        inv_hz_array =  np.genfromtxt(MF_dir+"h.txt",comments="#") ;# mult by H0 to get 1/H(z) !?!
        #ix = np.argsort(z_array)
        #z_array = z_array[ix]
        #r_array = r_array[ix]
        #dndm_array = dndm_array[ix]
        #inv_hz_array = inv_hz_array[ix]

        dvdz_array=np.genfromtxt(DISTANCES_dir+"z.txt", comments="#")
        dvdz=np.genfromtxt(DISTANCES_dir+"dvdz.txt", comments="#")
        ix = np.argsort(dvdz_array)
        dvdz_array = dvdz_array[ix]
        dvdz = dvdz[ix]
    
        #
        # Tinker formula will need volume
        #
        self.dvdz_interpolator = interp1d(dvdz_array, dvdz)
    
        #
        # Tinker formula will need dn/dM
        #
        rho_m=2.775e11*(self.om) # h^2 M_solar Mpc^-3.
        marray=(4.0*3.1415/3.0)*rho_m*r_array**3  # M_solar/h
        lnmarray = np.log(marray)
        dndm_array=dndm_array.reshape([np.size(a_array),np.size(marray)]).T
        # 2D interpolator into mass function
        self.mass_function_interpolator = RectBivariateSpline(lnmarray,a_array,dndm_array)
    

    def cluster_density(self, zed, m200):
        """
        m200 is in units of 10^14 M_sun
        """
        a = 1/(zed+1)
        m200 = m200*1e14
        lnM200 = np.log(m200)
        cluster_density_dmdz = self.mass_function_interpolator( lnM200, a) 
        return cluster_density_dmdz 

    def cluster_counts(self, zed, m200, del_zed, dm) :
        """
        m200 is in units of 10^14 M_sun
        """
        frac = self.fraction_sky
        dm = dm/m200
        volume = frac*4*np.pi * self.dvdz_interpolator(zed) * del_zed 
        cluster_density_dmdz = self.cluster_density(zed, m200)
        cluster_counts_model =cluster_density_dmdz * volume * dm * del_zed 
        return cluster_counts_model

