import os
import numpy as np
from cosmosis.datablock import option_section, names
from scipy.interpolate import interp1d
import one_halo

def setup(options):
     cons = 'test'
     return cons

def execute(block, config):
    """
    One halo term

    Convention:
        mass is in units of M_sun
        m200 is in units of 10^14 M_sun
    """
    #==================================
    #
    # cosmological parameters
    #
    #==================================
    cosmo = names.cosmological_parameters

    doCosmo = block[cosmo,"doCosmo"]

    omega_b = block[cosmo,"omega_b"]
    omega_m = block[cosmo,"omega_m"]

    if doCosmo:
        dis = names.distances
        z_table = block[dis,'z']
        da_table = block[dis,'d_a']
        hz_table = block[dis,'h']*3e5  ;# to get into km/s/Mpc

        # tinker mass function  in critical; to get background mult by omega_m
        mf = names.mass_function
        mz_table=block[mf,"z"] 
        m_table=block[mf,"m_h"]
        dndlnMh_table=block[mf,"dndlnMh"].reshape([np.size(mz_table),np.size(m_table)]).T

        # tinker bias
        tbf = "tinker_bias_function"
        tinker_bias_mass = block[tbf,"ln_mass"]
        tinker_bias_bias = block[tbf,"bias"]
        bias_interpolator = interp1d(tinker_bias_mass, tinker_bias_bias)

    else:
        cosmosis_dir = "output_cross/"
        z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
        da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
        hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5 

        mz_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/z.txt"), skip_header=1)
        m_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/m_h.txt"), skip_header=1)
        dndlnMh_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/dndlnmh.txt"), \
            skip_header=1).reshape([np.size(mz_table),np.size(m_table)]).T

        # tinker bias
        tinker_bias_mass = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/ln_mass.txt"), skip_header=1)
        tinker_bias_bias = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/bias.txt"), skip_header=1)
        bias_interpolator = interp1d(tinker_bias_mass, tinker_bias_bias)



    #==================================
    #
    # sz profile parameters
    #
    #==================================
    cosmo = names.cosmological_parameters
    Ap = block[cosmo,"Ap"]
    Bp = block[cosmo,"Bp"]
    Cp = block[cosmo,"Cp"]
    Ax = block[cosmo,"Ax"]
    Bx = block[cosmo,"Bx"]
    Cx = block[cosmo,"Cx"]
    Abeta = block[cosmo,"Abeta"]
    Bbeta = block[cosmo,"Bbeta"]
    Cbeta = block[cosmo,"Cbeta"]
    # and we will use Mechior et al 2016 eq 51 and table 4
    m200_pivot =  block[cosmo,"m200_pivot"] # in units of 1e14 M_sun
    Fm =  block[cosmo,"Fm"] # the lambda power law
    Cm =  block[cosmo,"Cm"] # the redshift power law
    sigma =  block[cosmo,"m_sigma"] # mass scatter

    #==================================
    #
    # sample parameters
    #
    #==================================
    cosmo = names.cosmological_parameters
    psf   = block[cosmo, "PSF"]
    fraction_of_sky = block[cosmo,"fraction_of_sky"]

    z_range = np.array( [[0.2, 0.4],[0.4,0.6],[0.6,0.8]])
    lam_range = np.array(([5,10], [10,14], [14,20], [20,35],[35,180])).astype(int)

    for zr in z_range:
        for lr in lam_range :
            zed1 = zr[0]
            zed2 = zr[1]
            lambda_low = lr[0]
            lambda_high = lr[1]

            phys_r_samples, h1_mass_weighted_profile, \
                h2_mass_weighted_profile, b_mu_bin  = \
                one_halo.main( zed1, zed2, fraction_of_sky, lambda_low, lambda_high,
                z_table, da_table, hz_table, omega_b, omega_m,
                dndlnMh_table, m_table, mz_table, 
                tinker_bias_mass, tinker_bias_bias,
                m200_pivot, Fm, Cm, sigma,
                Ap, Bp, Cp, Ax, Bx, Cx, Abeta, Bbeta, Cbeta) 

            #==================================
            #
            # Done. Output
            #
            #==================================
        
            sample= "mass_ge{}_lt{}_z_ge{:.1f}_lt{:.1f}".format(
                lr[0], lr[1], zr[0], zr[1])

            szb = "tSZ"
            block[szb,'phys_radius_one_{}'.format(sample)] = phys_r_samples
            block[szb,'profile_one_{}'.format(sample)] = h1_mass_weighted_profile
            block[szb,'phys_radius_two_sum_{}'.format(sample)] = phys_r_samples
            block[szb,'profile_two_sum_{}'.format(sample)] = h2_mass_weighted_profile
            block[szb,'b_mu_bin_{}'.format(sample)] = b_mu_bin

def cleanup(config):
    #nothing to do here!  We just include this 
    # for completeness
    return 0

