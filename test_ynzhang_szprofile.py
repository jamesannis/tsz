import os
import numpy as np
import matplotlib.pyplot as plt
import sz_profile
from scipy.interpolate import interp1d
cosmosis_dir = "output_cross/"
z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5

v2=np.genfromtxt('yproj_14.3_0.1.txt')
v3=np.genfromtxt('yprof3d_14.3_0.1.txt')

c=sz_profile.szProfile(0.1, 1.6265, z_table, da_table, hz_table)
radv, prov, projv=c.project_profile(v3[:, 0]/1.1, v3[:, 1]/251.4)
provd=provd*2.0

proj_radius, proj_profile, obs_theta, obs_profile=c.project_and_convolve_with_beam(v3[:, 0]/1.1, v3[:, 1]/251.4, 10.0, 0.1, z_table, da_table)

plt.plot(v2[:, 1]/1.1, v2[:, 2], 'r--')
plt.plot(radv, prov, 'k')
#plt.plot(v2[:, 1]/1.1, v2[:, 5], 'r--')
#plt.plot(proj_radius, obs_profile, 'k')
#plt.plot(proj_radius, obs_profile, 'b')
plt.xscale('log')
plt.yscale('log')
plt.show()

f=interp1d(radv, prov)
fv=interp1d(v2[:, 1]/1.1, v2[:, 2])
xx=np.arange(0.1, 5, 0.1)

plt.plot(xx, prov/provd)#f(xx)/fv(xx))
plt.xscale('log')
plt.show()



