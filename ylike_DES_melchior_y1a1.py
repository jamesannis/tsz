from cosmosis.datablock import option_section, names
import numpy as np
from scipy.interpolate import interp1d

def setup(options):
    #import fitsio
    import pyfits

    z_range = np.array( [[0.2, 0.4],[0.4,0.6],[0.6,0.8]])
    mass_range = np.array(([5,10], [10,14], [14,20], [20,35],[35,180])).astype(int)
    dir = "/data/des50.b/data/DESxSPT_shared/measurements/DES_melchior_y1a1/"
    file = "mass_ge{}_lt{}_z_ge{:.1f}_lt{:.1f}.fits"
    data = dict()
    for zr in z_range:
        for mr in mass_range:
            #fits = fitsio.FITS(dir+file.format(mr[0], mr[1], zr[0], zr[1]))
            #data[zr,mr,"cov"] =  fits[0][:,:]
            #data[zr,mr,"mes"] = fits[1]["R_norm"][:], fits[1]["meanR"][:], \
            #    fits[1]["meanlogR"][:], fits[1]["y"][:], \
            #    fits[1]["yerr_jacknife"][:], fits[1]["yerr_treecorr"][:]
            fits = pyfits.open(dir+file.format(mr[0], mr[1], zr[0], zr[1]))
            s = "{},{}".format(zr,mr)
            data[s,"cov"] = fits[0].data
            data[s,"mes"] = fits[1].data.field("R_norm"), fits[1].data.field("meanR"), \
                fits[1].data.field("meanlogR"), fits[1].data.field("y"), \
                fits[1].data.field("yerr_jacknife"), fits[1].data.field("yerr_treecorr")
    return z_range, mass_range, data

def execute(block, config):
    likes = names.likelihoods
  # data
    z_range, mass_range, data = config
 # theory   
    szb = "tSZ"

    ylike = 0
    for zr in z_range:
        for mr in mass_range:
            s = "{},{}".format(zr,mr)
            data_radius = data[s,"mes"][1] ;# "meanR"
            data_y = data[s,"mes"][3]      ;# "y"
            data_yerr = data[s,"mes"][4]   ;# "yerr_jacknife"
            cov = data[s,"cov"]

            sample= "mass_ge{}_lt{}_z_ge{:.1f}_lt{:.1f}".format(
                mr[0], mr[1], zr[0], zr[1])
            r = block[szb,'y_theta_{}'.format(sample)]
            y = block[szb,'y_profile_{}'.format(sample)]
            #last_point = r[-1]
            #first_point = r[0]
            #ix = (data_radius > first_point) & (data_radius < last_point)
            #data_radius = data_radius[ix]
            #data_y = data_y[ix]
            #data_yerr = data_yerr[ix]
            #ix = np.nonzero(np.invert(ix))[0]
            #cov = np.delete(cov, ix, 0) ;# rows
            #cov = np.delete(cov, ix, 1) ;# columns
            #inv_cov = np.linalg.inv(cov)
        
            ytheory = interp1d(r,y, bounds_error=False, fill_value="extrapolate")

            d = data_y - ytheory(data_radius)
            print "==========================",zr,mr
            #chi2 = np.einsum('i,ij,j', d, inv_cov, d)
            chi2 = ((d/data_yerr)**2).sum()
            print chi2
            ylike += -0.5*chi2

    block[likes, 'YLIKE_LIKE'] = ylike
    return 0
    
    
