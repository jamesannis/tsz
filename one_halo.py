import numpy as np
from scipy.interpolate import interp1d
from scipy.interpolate import RectBivariateSpline
import scipy.integrate

import scaling_relations
import cluster_average
import sz_profile



def main( zed1, zed2, fraction_of_sky, 
            lambda_low, lambda_high, z_table, da_table, hz_table,
            omega_b, omega_m,
            dndlnMh_table, m_table, mz_table, 
            tinker_bias_ln_mass, tinker_bias_zed, tinker_bias_bias,
            m200_pivot, Fm, Cm, sigma,
            Ap, Bp, Cp, Ax, Bx, Cx, Abeta, Bbeta, Cbeta) :
    """
    One halo term

    Convention:
        mass is in units of M_sun
        m200 is in units of 10^14 M_sun
    """
    zed = (zed1+zed2)/2.

    area = 4.0*np.pi*fraction_of_sky  # fraction of the sky covered, taking mass function as per sterdadian
    dndlnMh_table = dndlnMh_table * area  # numbers inside survey area
    #2D interpolator into mass function; mass in linear in solar masses
    mass_function = RectBivariateSpline(m_table,mz_table,dndlnMh_table)

    m200_obs_low = scaling_relations.mass_from_lambda (m200_pivot, Fm, Cm, zed, lambda_low) 
    m200_obs_hi  = scaling_relations.mass_from_lambda (m200_pivot, Fm, Cm, zed, lambda_high) 

    # reasonable mass integration limits
    limit_low = np.log(1e12)
    limit_hi =  np.log(1e16)

    bias_interpolator_2d = RectBivariateSpline(\
         tinker_bias_zed, tinker_bias_ln_mass, tinker_bias_bias)
    bias = bias_interpolator_2d(zed, tinker_bias_ln_mass)
    bias_interpolator = interp1d(tinker_bias_ln_mass, bias, \
        bounds_error=False, fill_value="extrapolate")



    #==================================
    #
    # main computation
    #
    #==================================

    # The primary goal is to make mass weighted 3-d SZ profiles
    # 
    # This will be done by building, at a variety of points in r_samples
    # an interpolator of the profile in mass, for that radius
    #
    
    # Build the interpolators (there is a set of them, as there are many radii)
    #       the points in r_samples are ln(phys_radius)
    r_samples, phys_r_samples , xi_at_r = \
        build_profile_interpolator( limit_low, limit_hi, zed, 
        Ap, Bp, Cp, Ax, Bx, Cx, 
        Abeta, Bbeta, Cbeta, z_table, da_table, hz_table, omega_b, omega_m)

    # we need a mass averager object, which does the integrals for us
    mass_averager = cluster_average.expectation(zed, mass_function)

    # first we'll build the 1-halo mass averaged profile within lambda bin
    h1_mass_weighted_profile = []
    for sr in r_samples:
        h1_mwp = mass_averager.value_interp_lambda_bin(
            xi_at_r[sr], m200_obs_low, m200_obs_hi, sigma)
        h1_mass_weighted_profile.append(h1_mwp)
        n_halo_bin = mass_averager.norm
    # Vikram, Lindz, Jain eq 19 (with norm in mass_averager)
    h1_mass_weighted_profile = np.array(h1_mass_weighted_profile)

    # then for later use in 2-halo we'll build the mass averaged 3-d profile x bias
    h2_mass_weighted_profile = []
    for sr in r_samples:
        h2_mwp = mass_averager.value_interp2(
            xi_at_r[sr], bias_interpolator)
        n_halo = mass_averager.norm
        h2_mass_weighted_profile.append(h2_mwp*n_halo)
    h2_mass_weighted_profile = np.array(h2_mass_weighted_profile)

    # also for latter use in the 2-halo term, we'll build
    #   <b_mu [erfc(x_i) - erfc(x_{i+1})]>
    b_mu_bin = mass_averager.value_interp_lambda_bin(
            bias_interpolator, m200_obs_low, m200_obs_hi, sigma)

    return  phys_r_samples, h1_mass_weighted_profile, h2_mass_weighted_profile, b_mu_bin  

def build_profile_interpolator(limit_low, limit_hi, zed, Ap, Bp, Cp, Ax, Bx, Cx,
        Abeta, Bbeta, Cbeta, z_table, da_table, hz_table, omega_b, omega_m) :

    # then we'll need the weighted summed profile
    # this is a mess, as the sz profile (aka cluster-sz correlation function)
    # is a vector.
    # What we'll do is to evaluate SZ at a variety (a few) masses
    # build interpolators on mass for a variety of radii,
    # do the mass integrals on those interpolators 
    # (on the correlation function for particular radii)
    # and then (later, in ylike) build an interpolator on radius for the sz profile
    # from the mass weighted integraled values.
    k = 3
    sz_data = dict()
    m_samples = np.linspace(limit_low, limit_hi, 2**k)
    for sm in m_samples:
        s_m200 = np.exp(sm)/1e14
        sz_data[sm]  = sz_profile.szProfile(zed,s_m200,z_table, da_table, 
            hz_table, omega_b, omega_m, Ap=Ap, Bp=Bp, Cp=Cp, Ax=Ax, Bx=Bx, Cx=Cx,
            Abeta=Abeta, Bbeta=Bbeta, Cbeta=Cbeta)

    # the xi, which is the 3d tSZ profile
    xi_at_r = dict()
    k = 4
    pixel_scale = sz_data[sm].pixel_scale 
    maxRad = sz_data[sm].maxRadius
    r_samples = np.linspace(np.log(pixel_scale), np.log(maxRad-.1), 2**k)
    phy_r_samples = []
    for sr in r_samples:
        d3_profile_at_r = []
        s_rad = np.exp(sr)
        for sm in m_samples:
            phys_rad = sz_data[sm].radial
            three_d_prof = sz_data[sm].phys_profile

            d3_interp = interp1d(phys_rad, three_d_prof, bounds_error=False, fill_value="extrapolate")
            d3_profile_at_r.append(d3_interp(s_rad))
        
            #print s_rad, np.log10(np.exp(sm)),
            #print "    ", phys_rad[1], phys_rad[-1], s_rad
        #print s_rad, np.array(profile_at_r)
        xi_at_r[sr] = interp1d(m_samples, np.array(d3_profile_at_r) )
        phy_r_samples.append( s_rad )
    phy_r_samples = np.array(phy_r_samples)
    return r_samples, phy_r_samples , xi_at_r


def calc_press (m200, zed, Ap, Bp, Cp, z_table, da_table, hz_table, omega_b, omega_m ) :
    sz = sz_profile.szProfile(zed,m200,z_table, da_table, hz_table, omega_b, omega_m,
        Ap=Ap, Bp=Bp, Cp=Cp)
    pressure = sz.pressure
    return pressure


def build_press_interp(limit_low, limit_hi, zed, Ap, Bp, Cp, 
        z_table, da_table, hz_table, omega_b, omega_m) :
    k = 3
    sz_data = []
    m_samples = np.linspace(limit_low, limit_hi, 2**k)
    for sm in m_samples:
        s_m200 = np.exp(sm)/1e14
        sz_data.append(calc_press(s_m200, zed, Ap, Bp, Cp,
            z_table, da_table, hz_table, omega_b, omega_m))

    press_data = np.array(sz_data)
    return m_samples, press_data
