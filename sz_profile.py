import numpy as np
import os
import scaling_relations
from scipy.interpolate import interp1d

class szProfile(object):
    """szProfile

        Create an object that will be used to model the SZ Y profile 
        m200 is in units of 10^14 M_sun
        psf_sigma is in arcminutes, the PSF is assumed to be a circularly symmetric gaussian

        z_table, da_table, h_table are tables of redshift, angular diameter distance, and h(z)
        as given by cosmosis (i.e, they are ordered by a= 1/(1+z)

        The default parameters are from table 1 of Battaglia, Bond, Pfrommer, Sievers 2011 
        arXiv:1109.3711

source activate /data/des50.b/data/DESxSPT_shared/cosmo2016/
"""
    def __init__(self, 
        zed, m200, z_table, da_table, h_table, 
        omega_b=0.047, omega_m=0.286,
        Ap=18.1, Bp=0.154, Cp=-0.758,
        Ax=0.497, Bx=-0.00865, Cx=0.731,
        Abeta=4.35, Bbeta=0.0393, Cbeta=0.415
        ):
        # changing the pixel scale from 0.2 to 0.02 raises the central convolved
        # value by a factor of 1.4-1.5; from 0.05 to 0.02 factors 1.1-1.2
        #pixel_scale=0.1; 
        #maxRadius = 25.
        pixel_scale=0.15; # Mpc/pixel
        maxRadius = 15.   ;# Mpc
        self.Ap = Ap
        self.Bp = Bp
        self.Cp = Cp
        self.Ax = Ax
        self.Bx = Bx
        self.Cx = Cx
        self.Abeta = Abeta
        self.Bbeta = Bbeta
        self.Cbeta = Cbeta
        self.zed = zed
        self.m200 = m200
        self.set_cosmology(z_table, da_table, h_table)

        hz =  self.hz_interpolator(zed)
        constants = self.pressure_constants()
        self.pressure = constants \
                      * scaling_relations.p200_from_mass(hz,m200,omega_b,omega_m) \
                      * scaling_relations.f_from_Y() \
                      * scaling_relations.pressure_from_mass (Ap, Bp, Cp, zed, m200) 
        self.xc       = scaling_relations.xc_from_mass(Ax, Bx, Cx, zed, m200)
        self.beta     = scaling_relations.beta_from_mass(Abeta, Bbeta, Cbeta, zed, m200)
        self.r200     = scaling_relations.r200_from_mass(hz,m200) 

        self.pixel_scale=pixel_scale
        self.maxRadius = maxRadius
        self.image_size = np.round(maxRadius*2/pixel_scale) ;# max is radius, image is diameter
    
        self.set_phys_profile()

    def set_phys_profile(self) :
        xc = self.xc
        beta = self.beta
        image_size = self.image_size
        pixel_scale = self.pixel_scale ;# mpc/pixel
        r200 = self.r200 # in units of mpc

        #print "np.arange(0,",(image_size/2.)+1,") *",pixel_scale, "/",r200, "      xc=",xc
        radius = np.arange(0,(image_size/2.)+1) * pixel_scale
        radius[radius == 0] =  0.1 * pixel_scale
        phys_profile = self.profile_at_radius(radius/r200, xc, beta)   
        if np.nonzero(phys_profile <= 0)[0].size > 0 : 
            raise Exception("negative phys profile")

        ix = radius <= self.maxRadius
        self.radial = radius[ix]
        self.phys_profile = phys_profile[ix] * self.pressure


    # radius is x, that is, in units of r200
    # equation 30
    def profile_at_radius(self, x, xc, beta)  :
        profile = (x/xc)**-0.3 * (1+(x/xc))**-beta
        return profile


    def set_cosmology(self, z_array, da_array, h_array) :
        from scipy.interpolate import interp1d
        ix = np.argsort(z_array)
        z_array = z_array[ix]

        da = da_array[ix]/(60.*(360./2/np.pi))  # in Mpc/arcminutes
        da_interpolater = interp1d(z_array, da)
        self.da_interpolator = da_interpolater

        h_array = h_array[ix]
        hz_interpolater = interp1d(z_array, h_array)
        self.hz_interpolator = hz_interpolater

#
# if one is working with frequency dependent maps, one uses this routine
#   both Planck and SPT provide Y maps, so this routine is not needed.
#
    def gnu (self) :
        """Frequency dependence
        g(nu) = x coth(x/2)-4 where x = h\nu/kT_{cmb} T_{cmb}=2.7255K
This is useful for calculating the contribution of tSZ to a frequency map,
but is taken out during the construction of the Y maps; 
indeed, it is central to that construction.

        nu is in units of GHZ
        """
        x = 1.761 * self.nu/100.
        g_nu = x * ( (np.e**(x/2) + np.e**(-x/2))/(np.e**(x/2) - np.e**(-x/2))) - 4
        return g_nu

#
# scale to y using constants
#
    def pressure_constants(self):
        # eq 3
        # thompson cross section/m_e/c^2 = 8.125532e-16 s^2/kg
        # 1.988e30 kg/solar mass  => 1.615356e15 s^2/solar mass
        constants = 1.615356e15 # this has units s^2/M_sun  
        return constants


    def project_and_convolve_with_beam(self, radius, profile, psf_fwhm, zed, z_array, da_array) :
        # 3d to 2d using abel transform
        proj_radius, proj_profile = self.project_profile(radius, profile)
        proj_image = self.make_proj_image(proj_radius, proj_profile)
        # convolve with observation PSF and change radius to angle
        obs_theta, obs_profile = self.observe_profile(radius, proj_image, psf_fwhm, 
            zed, z_array, da_array)
        return proj_radius, proj_profile, obs_theta, obs_profile
    
    def project_profile(self, radius, profile) :
        if np.nonzero(profile <= 0)[0].size > 0 : raise Exception("negative profile")
        max_radius = self.maxRadius
        profile_spline = interp1d(radius, profile, 
            bounds_error=False, fill_value="extrapolate")
        dr = 0.001
        rmin = 0.
        space = int(1 + (max_radius - rmin) / dr)
        sqxi = np.linspace(rmin, max_radius, space)**2.
        profile_projected = []
        for r in radius:
            R = np.sqrt(r**2. + sqxi)
            R = R[R < max_radius]
            profile_projected.append(profile_spline(R).sum()*dr)
        profile_projected = np.array(profile_projected)
        new_radius = radius 
        return new_radius, profile_projected

    def make_proj_image(self, radius, profile) :
        image_size = self.image_size
        pixel_scale = self.pixel_scale
        profile_spline = interp1d(radius, profile, 
            bounds_error=False, fill_value="extrapolate")

        image = np.zeros( ((image_size/2)+1, (image_size/2)+1))
        rcen, ccen = 0,0
        for r in range(0,np.shape(image)[0]) :
            for c in range(0,np.shape(image)[1]) :
                radius = np.sqrt((r-rcen)**2+(c-ccen)**2) * pixel_scale
                if radius == 0. : radius = 0.1 * pixel_scale
                image[r][c] = profile_spline(radius)
        return image
        

    def observe_profile(self, radius, projected_image, psf_fwhm, zed, z_array, da_array) :
        pixel_scale = self.pixel_scale
        da = get_da(z_array, da_array) 
        #print "psf arcmin",psf_fwhm,
        psf_fwhm_mpc = psf_fwhm * da(zed) ;# arcmin * Mpc/arcmin
        #print "psf mpc",psf_fwhm_mpc, 
        psf_fwhm_pixels = psf_fwhm_mpc / pixel_scale ;# Mpc /  (Mpc/pixel)
        #print "psf pixels",psf_fwhm_pixels
        psf_sigma = psf_fwhm_pixels*0.425 # FWHM != gaussian sigma
    
        obs_profile = projected_image[0,:]
        obs_theta = np.arange(0,obs_profile.size) * pixel_scale/da(zed)
        #print "----- skipping convolve"
        #obs_theta=radius
        #return obs_theta, obs_profile
    
        convolved_image = psf_convolve(psf_sigma, projected_image)
        obs_profile = convolved_image[0,:]
        obs_theta = np.arange(0,obs_profile.size) * pixel_scale/da(zed)
        return obs_theta, obs_profile
    
# assume PSF is 2-d gaussian
def psf_convolve(sigma, projection) :
    from astropy.convolution import convolve_fft
    from astropy.convolution import Gaussian2DKernel
    gauss = Gaussian2DKernel(stddev=sigma)
    full_proj = mirror(projection)
    convolved_projection = convolve_fft(full_proj, gauss)
    shape = np.shape(projection)
    # get lower right quadrant
    convolved_projection = convolved_projection[shape[0]:,shape[1]:]
    return convolved_projection

def mirror(image):
    colflip = np.fliplr(image[:,0:])
    image2 = np.hstack((colflip, image))
    rowflip = np.flipud(image2[0:,:])
    image3= np.vstack((rowflip,image2))
    return image3

def get_da(z_array, da_array) :
    from scipy.interpolate import interp1d
    ix = np.argsort(z_array)
    z_array = z_array[ix]

    da = da_array[ix]/(60.*(360./2/np.pi))  # in Mpc/arcminutes
    da_interpolater = interp1d(z_array, da)
    return da_interpolater

