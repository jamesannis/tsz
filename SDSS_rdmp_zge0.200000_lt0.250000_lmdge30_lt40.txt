# # meanR, Y, Y_sigma
1.418900e-01 7.164311e-07 6.701033e-07
2.162100e-01 1.086173e-06 3.750025e-07
3.495100e-01 9.283288e-07 1.551696e-07
6.053000e-01 1.196688e-06 1.481286e-07
9.979000e-01 1.144365e-06 1.148523e-07
1.650800e+00 1.076251e-06 9.364748e-08
2.712700e+00 9.960195e-07 7.232509e-08
4.473000e+00 8.168697e-07 5.684851e-08
7.375100e+00 4.907787e-07 4.123535e-08
1.216200e+01 1.974365e-07 3.231121e-08
2.004800e+01 5.148040e-08 2.915609e-08
3.305500e+01 1.762124e-08 2.209863e-08
5.449800e+01 7.793675e-10 1.911190e-08
8.985600e+01 2.702039e-08 1.964264e-08
1.481500e+02 2.305354e-08 1.208398e-08
2.442900e+02 3.056636e-09 8.656726e-09
4.029200e+02 2.929438e-09 5.440745e-09
6.649500e+02 -4.251187e-09 4.135840e-09
