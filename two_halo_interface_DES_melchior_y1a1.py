import os
import numpy as np
from cosmosis.datablock import option_section, names
import two_halo

def setup(options):
     cons = 'test'
     return cons

def execute(block, config):
    """
    Two halo term

    Convention:
        mass is in units of M_sun
        m200 is in units of 10^14 M_sun
    """
    cons = config

    #==================================
    #
    # cosmological parameters
    #
    #==================================
    cosmo = names.cosmological_parameters
    doCosmo = block[cosmo,"doCosmo"]

    if doCosmo:
        # matter-matter power spectrum
        mps = "matter_power_lin"
        k_h = block[mps, "k_h"]
        pk_z_table = block[mps, "z"]
        p_k_table=block[mps,"p_k"].reshape([np.size(pk_z_table),np.size(k_h)]).T

    else:
        cosmosis_dir = "output_cross/"
        # matter-matter power spectrum
        pk_z_table = np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/z.txt"), skip_header=1)
        k_h = np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/k_h.txt"), skip_header=1)
        p_k_table=np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/p_k.txt"), \
            skip_header=1).reshape([np.size(pk_z_table),np.size(k_h)]).T

    #==================================
    #
    # sample parameters
    #
    #==================================
    z_range = np.array( [[0.2, 0.4],[0.4,0.6],[0.6,0.8]])
    lam_range = np.array(([5,10], [10,14], [14,20], [20,35],[35,180])).astype(int)

    for zr in z_range:
        for lr in lam_range :
            zed1 = zr[0]
            zed2 = zr[1]
            lambda_low = lr[0]
            lambda_high =  lr[1]
            sample= "mass_ge{}_lt{}_z_ge{:.1f}_lt{:.1f}".format(
                lr[0], lr[1], zr[0], zr[1])

            #==================================
            #
            # one-halo results
            #
            #=================================
            szb = "tSZ"
            doCosmo = True;# surely this was never supposed to be here?
            # do cosmo is about running camb
            if doCosmo:
                phys_r_samples = block[szb,'phys_radius_two_sum_{}'.format(sample)] 
                h2_mass_weighted_profile = block[szb,'profile_two_sum_{}'.format(sample)] 
                h1_mass_weighted_profile = block[szb,'profile_one_{}'.format(sample)] 
                #  bias in a lambda bin to multiply the matter-matter power spectrum by
                b_mu_bin = block[szb,'b_mu_bin_{}'.format(sample)] 
            else :
                phys_r_samples = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/phys_radius_two_sum_{}.txt".format(sample)), skip_header=1)
                h2_mass_weighted_profile = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/profile_two_sum_{}.txt".format(sample)), skip_header=1)
                h1_mass_weighted_profile = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/profile_one_{}.txt".format(sample)), skip_header=1)
        
                #  bias in a lambda bin to multiply the matter-matter power spectrum by
                print "SURELY BROKEN!!! Where is the b_mu_bin value?"
                keyword = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/values_{}.txt".format(sample)), unpack=True,usecols=0,dtype="str")
                values = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/values_{}.txt".format(sample)), unpack=True,usecols=2)
                ix = keyword == "b_mu_bin"
                b_mu_bin = values[ix]
        
        
            #==================================
            #
            # main computation
            #
            #=================================
        
            r_samples, corr, krange, p_k_sz = two_halo.main( \
                zed1, zed2, phys_r_samples, 
                h1_mass_weighted_profile, h2_mass_weighted_profile, b_mu_bin,
                k_h, pk_z_table, p_k_table) 
        
            #==================================
            #
            # Done. Output
            #
            #=================================
            block[szb,'phys_radius_two_{}'.format(sample)] = r_samples
            block[szb,'profile_two_{}'.format(sample)] = corr
            block[szb,'k_{}'.format(sample)] = krange
            block[szb,'pk_{}'.format(sample)] = p_k_sz

def cleanup(config):
    return 0

