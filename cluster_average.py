import numpy as np
import scipy.integrate
import scipy.special

class expectation(object):
    """expectation value

   We define an expectation value of x, <x>, as
   <x> = \int_0^\infty dm/M rho(M) x
    where rho(M) is the cluster mass function in dn/dlnM
    and M is the cluster mass

    We are working in the single redshift limit.

    mass_function is an interpolator over a dndlnMh array,
        likely a RectBivariateSpline

    Conventions:
        mass is in units of M_sun
        m200 is in units of 10^14 M_sun
"""
    def __init__(self, zed, dndlnMh_interpolator, mlow=1e12, mhi=1e16) :
        self.zed = zed
        self.dndlnm = dndlnMh_interpolator
        self.limit_low = np.log(mlow)
        self.limit_hi =  np.log(mhi)

        # get normalization
        nclusters =  self.get_norm()
        self.norm = nclusters

    def get_norm(self) :
        # get normalization
        ans = scipy.integrate.quad(ev_integrand, self.limit_low, self.limit_hi, 
            args=(self.zed, self.dndlnm, 1.0) )
        expectation_value = ans[0]; error = ans[1]
        return expectation_value


# <x>
    def value(self, x) :
        ans = scipy.integrate.quad(ev_integrand, self.limit_low, self.limit_hi, 
            args=(self.zed, self.dndlnm, x) )
        expectation_value = ans[0]; error = ans[1]
        return expectation_value / self.norm

# <x(ln mass)>
# e.g.,     <g_mu(r)>            this is for notes eq  32
#           <b_mu>
    def value_interp(self, x) :
        ans = scipy.integrate.quad(ev_interp, self.limit_low, self.limit_hi, 
            args=(self.zed, self.dndlnm, x) )
        expectation_value = ans[0]; error = ans[1]
        return expectation_value / self.norm

# <mass>
    def value_mass(self) :
        ans = scipy.integrate.quad(ev_mass, self.limit_low, self.limit_hi, 
            args=(self.zed, self.dndlnm ) )
        expectation_value = ans[0]; error = ans[1]
        return expectation_value / self.norm


# <x(ln mass) y(ln_mass)>
#       e.g., <b_mu P_mu>
    def value_interp2(self, x, y) :
        ans = scipy.integrate.quad(ev_interp2, self.limit_low, self.limit_hi, 
            args=(self.zed, self.dndlnm, x, y) )
        expectation_value = ans[0]; error = ans[1]
        return expectation_value / self.norm

#
# inside a lambda bin
#
    def lambda_bin_norm(self, m200_obs_low, m200_obs_hi, sigma_mass) :
        ans = scipy.integrate.quad(ev_x_lambda_bin, 
            self.limit_low, self.limit_hi, 
            args=(self.zed, self.dndlnm, 
            m200_obs_low, m200_obs_hi, sigma_mass, 1.0) )
        expectation_value = ans[0]; error = ans[1]
        return expectation_value

# <x [erfc(x_i) - erfc(x_{i+1})]>
    def value_x_lambda_bin(self, x, 
            m200_obs_low, m200_obs_hi, sigma_mass) :
        ans = scipy.integrate.quad(ev_x_lambda_bin, 
            self.limit_low, self.limit_hi, 
            args=(self.zed, self.dndlnm, 
            m200_obs_low, m200_obs_hi, sigma_mass, x) )
        expectation_value = ans[0]; error = ans[1]
        nclusters = self.lambda_bin_norm(m200_obs_low, m200_obs_hi, sigma_mass) 
        return expectation_value/nclusters

# <x(ln mass) [erfc(x_i) - erfc(x_{i+1})]>
# e.g, <P_mu G_mu(r) [erfc(x_i) - erfc(x_{i+1})]>  notes eq 24 and techinical note
    def value_interp_lambda_bin(self, x, 
            m200_obs_low, m200_obs_hi, sigma_mass) :
        ans = scipy.integrate.quad(ev_interp_lambda_bin, 
            self.limit_low, self.limit_hi, 
            args=(self.zed, self.dndlnm, 
            m200_obs_low, m200_obs_hi, sigma_mass, x) )
        expectation_value = ans[0]; error = ans[1]
        nclusters = self.lambda_bin_norm(m200_obs_low, m200_obs_hi, sigma_mass) 
        return expectation_value/nclusters

# <ln mass [erfc(x_i) - erfc(x_{i+1})]>
    def value_mass_lambda_bin(self) :
        ans = scipy.integrate.quad(ev_mass_lambda_bin, self.limit_low, self.limit_hi, 
            args=(self.zed, self.dndlnm ) )
        expectation_value = ans[0]; error = ans[1]
        return expectation_value / self.norm

# we'll do the integral in ln_mass rather than d mass/mass
# notes, eq 26 (mass weighted expectation value of x)
# <x>
def ev_integrand(ln_mass, zed, dndlnMh, x):
    halo_rho = dndlnMh(np.exp(ln_mass),zed)
    integrand = halo_rho * x
    return integrand

# <x(ln mass)>
def ev_interp(ln_mass, zed, dndlnMh, x):
    halo_rho = dndlnMh(np.exp(ln_mass),zed)
    integrand = halo_rho * x(ln_mass)
    return integrand

# <x(ln mass) y(ln_mass)>
def ev_interp2(ln_mass, zed, dndlnMh, x, y):
    halo_rho = dndlnMh(np.exp(ln_mass),zed)
    x_mu = x(ln_mass)
    y_mu = y(ln_mass)
    integrand = halo_rho * x_mu * y_mu
    return integrand

def ev_mass(ln_mass, zed, dndlnMh):
    halo_rho = dndlnMh(np.exp(ln_mass),zed)
    integrand = halo_rho * ln_mass
    return integrand

# < x [erfc(x_i) - erfc(x_{i+1})]>
# Lima Hu 2005, eq 4
def ev_x_lambda_bin(ln_mass, zed, dndlnMh, \
        m200_obs_low, m200_obs_hi, sigma_mass, x) :

    mass_bin = lh4(ln_mass, m200_obs_low, m200_obs_hi, sigma_mass) 
    halo_rho = dndlnMh(np.exp(ln_mass),zed)

    integrand = halo_rho * mass_bin * x
    return integrand

# < x(ln mass) [erfc(x_i) - erfc(x_{i+1})]>
# i.e, <P_mu G_mu(r) [erfc(x_i) - erfc(x_{i+1})]>  notes eq 24 and techinical note
# using the idea of Lima-Hun 2005, eq4
def ev_interp_lambda_bin(ln_mass, zed, dndlnMh, \
        m200_obs_low, m200_obs_hi, sigma_mass, x) :

    mass_bin = lh4(ln_mass, m200_obs_low, m200_obs_hi, sigma_mass) 
    halo_rho = dndlnMh(np.exp(ln_mass),zed)

    integrand = halo_rho * mass_bin * x(ln_mass)
    return integrand

# < ln mass [erfc(x_i) - erfc(x_{i+1})]>
def ev_mass_lambda_bin(ln_mass, zed, dndlnMh, \
        m200_obs_low, m200_obs_hi, sigma_mass) :

    mass_bin = lh4(ln_mass, m200_obs_low, m200_obs_hi, sigma_mass) 
    halo_rho = dndlnMh(np.exp(ln_mass),zed)

    integrand = halo_rho * mass_bin * ln_mass
    return integrand


# the Lima-Hu trick
def lh4(ln_mass,  m200_obs_low, m200_obs_hi, sigma_mass) :
    x1 = (np.log(m200_obs_low*1e14) - ln_mass) / np.sqrt(2)/sigma_mass
    x2 = (np.log(m200_obs_hi*1e14 ) - ln_mass) / np.sqrt(2)/sigma_mass
    efc1 = scipy.special.erfc(x1)
    efc2 = scipy.special.erfc(x2)
    return 0.5 * (efc1 - efc2)  

