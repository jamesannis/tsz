import os
import numpy as np
import hankel
import two_halo
from scipy.interpolate import interp1d
from scipy.interpolate import RectBivariateSpline
import matplotlib.pyplot as plt

def do(z1,z2,plot=True, bias=-999, vhm=True):
    import configparser

    cosmosis_dir = "output_cross/"
    config = configparser.ConfigParser()
    config.read('y_cross_rm_values.ini')
    cosmo = "cosmological_parameters"
    h0 = config.getfloat(cosmo,"h0")
    omega_b = config.getfloat(cosmo,"omega_b")
    omega_m = config.getfloat(cosmo,"omega_m")
    psf_fwhm = config.getfloat(cosmo,"PSF")
    zed1 = z1
    zed2 =  z2
    zed = (zed1+zed2)/2.

    #==================================
    #
    # cosmological parameters
    #
    #==================================

    # matter-matter power spectrum
    pk_z_table = np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/z.txt"), skip_header=1)
    k_h = np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/k_h.txt"), skip_header=1)
    p_k_table=np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/p_k.txt"), skip_header=1).reshape([np.size(pk_z_table),np.size(k_h)]).T
    p_k = RectBivariateSpline(k_h,pk_z_table,p_k_table)

    z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
    da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
    hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5 

    # tinker mass function  in critical; to get background mult by omega_m
    mz_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/z.txt"), skip_header=1)
    m_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/m_h.txt"), skip_header=1)
    dndlnMh_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/dndlnmh.txt"), skip_header=1).reshape([np.size(mz_table),np.size(m_table)]).T

    # tinker bias
    tinker_bias_ln_mass = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/ln_mass.txt"), skip_header=1)
    tinker_bias_zed = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/z.txt"), skip_header=1)
    tinker_bias_bias = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/bias.txt"), skip_header=1)

    bias_interpolator_2d = RectBivariateSpline(  
        tinker_bias_zed, tinker_bias_ln_mass, tinker_bias_bias)
    bias_at_z = bias_interpolator_2d(zed,tinker_bias_ln_mass).flatten()
    poly_coeff = np.polyfit( tinker_bias_ln_mass, np.log(bias_at_z), 3)
    #ix = tinker_bias_ln_mass > np.log(1e13)
    #bias_interpolator = interp1d(tinker_bias_ln_mass[ix], bias[ix])
    #poly_coeff = np.polyfit( tinker_bias_ln_mass[ix], np.log(bias[ix]), 5)
    #ix = tinker_bias_ln_mass > np.log(3e14)
    #bias_interpolator = interp1d(tinker_bias_ln_mass[ix], bias[ix])
    #poly_coeff = np.polyfit( tinker_bias_ln_mass[ix], np.log(bias[ix]), 1)

    #==================================
    #
    # one-halo results
    #
    #=================================
    szb = "tSZ"
    phys_r_samples = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/phys_radius_two_sum.txt"), skip_header=1)
    h2_mass_weighted_profile = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/profile_two_sum.txt"), skip_header=1)
    h1_mass_weighted_profile = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/profile_one.txt"), skip_header=1)
    #  bias in a lambda bin to multiply the matter-matter power spectrum by
    
    if bias == -999 :
        if not os.path.exists(os.path.join(cosmosis_dir,"tsz/values.txt")) : raise Exception("no tsz/values.txt file, give bias by hand")
        keyword = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/values.txt"), unpack=True,usecols=0,dtype="str")
        values = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/values.txt"), unpack=True,usecols=2)
        ix = keyword == "b_mu_bin"
        b_mu_bin = values[ix][0]
        if b_mu_bin.size == 0: raise Exception("empty b_mu_bin in tsz/values.txt")
    else :
        b_mu_bin = bias

    #==================================
    #
    # main computation
    #
    #=================================

    if not vhm :
        r_samples, corr, krange, p_k_sz, phys_r_samples,  \
            h1_mass_weighted_profile, h2_mass_weighted_profile,  \
            p_k_matter, p_k_1h, p_k_2h, mmcorr, h1corr, h2corr = \
            two_halo.main( zed1, zed2, phys_r_samples, 
                h1_mass_weighted_profile, h2_mass_weighted_profile, 
                b_mu_bin, k_h, pk_z_table, p_k_table, testing=True)
    
        if plot :
            plt.clf(); 
            ax=plt.subplot(1,1,1);ax.set_yscale("log"); ax.set_xscale("log"); 
            plt.plot(r_samples,corr);
            plt.plot(r_samples,h1corr,c="r");
            plt.plot(r_samples,h2corr,c="g")
            plt.scatter(phys_r_samples, h1_mass_weighted_profile,c="yellow")
            plt.xlabel("radius (log(Mpc))")
            plt.ylabel("log(Y)")
     
        #return r_samples, corr, krange, p_k_sz, phys_r_samples,  \
        #    h1_mass_weighted_profile, h2_mass_weighted_profile,  \
        #    p_k_matter, p_k_1h, p_k_2h, mmcorr, h1corr, h2corr 
        return r_samples, corr, phys_r_samples,  \
            h1_mass_weighted_profile, h1corr, h2corr 
    else :
        r_samples, y_projected  = \
            two_halo.main2( zed1, zed2, b_mu_bin, k_h, pk_z_table, p_k_table, \
            dndlnMh_table, m_table, mz_table, z_table, hz_table, 
            tinker_bias_zed, tinker_bias_ln_mass, tinker_bias_bias,
            h0, omega_b, omega_m)

        if plot :
            plt.clf(); 
            ax=plt.subplot(1,1,1);ax.set_yscale("log"); ax.set_xscale("log"); 
            plt.plot(r_samples,y_projected,c="red");
            plt.scatter(r_samples,y_projected,c="k"); plt.ylim(1e-7,1e-2)
            plt.xlabel("radius (log(Mpc))")
            plt.ylabel("log(Y)")
     
    
    if not vhm :
        return r_samples, h2_corr, h2_corr_sm
    else:
        return r_samples, y_projected

    
# reload(th);reload(oh); pr,h1,h2,b,ot,op= oh.do(doPlot=False);r,c,k,pk,pr,h1,h2,pkm,pk1,pk2,mmc,h1c,h2c=th.do(doPlot=False); t1,tp1,t2,tp2,t2m,tp2m,yr,yp=th.y(r,c,h1c,h2c,pr,h1)

def y_old(z1,z2,r_samples, corr, h1corr, h2corr, phys_r_samples, h1mwp, data="sdss.txt") :
    import sz_profile
    import configparser

    cosmosis_dir = "output_cross/"
    z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
    da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
    hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5 
    config = configparser.ConfigParser()
    config.read('y_cross_rm_values.ini')
    cosmo = "cosmological_parameters"
    psf_fwhm   = config.getfloat(cosmo, "PSF")
    zed1 = z1
    zed2 =  z2
    zed = (zed1+zed2)/2.

    ix = r_samples <= 20.
    one_halo_radius = r_samples[ix]
    two_halo_radius = r_samples[ix]
    one_halo_y = h1corr[ix]
    two_halo_y = h2corr[ix]
    two_halo_ym = corr[ix]

    # the corr function and rad profile have slightly different central amplitudes
    # try replacing corr with h1mpw to see  (phys_r_samples, h1mpw)
    one_halo_y = interp1d(phys_r_samples, h1mwp, \
        bounds_error=False, fill_value="extrapolate")(r_samples[ix])
    #one_halo_y = interp1d(r_samples, corr, \
    #    bounds_error=False, fill_value="extrapolate")(r_samples[ix])

    one_halo = interp1d(one_halo_radius, one_halo_y, bounds_error=False, fill_value="extrapolate")
    two_halo = interp1d(two_halo_radius, two_halo_y, bounds_error=False, fill_value="extrapolate")
    two_halo_ym = interp1d(two_halo_radius, two_halo_ym, bounds_error=False, fill_value="extrapolate")

    #==================================
    #
    # main computation
    #
    #==================================
    
    m200 = 1 ;# this won't be used in the project_and_convolve_with_beam
    sz = sz_profile.szProfile(zed,m200,z_table, da_table, hz_table)

    halo = one_halo(one_halo_radius) 
    ix = halo <= 0
    halo[ix] = halo[np.invert(ix)].min()
    proj_radius, proj_profile_one, one_theta, one_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 
    proj_radius, one_proj_profile = sz.project_profile(one_halo_radius, halo)

    halo = two_halo(one_halo_radius) 
    proj_radius, proj_profile_two, two_theta, two_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 
    proj_radius, two_proj_profile = sz.project_profile(one_halo_radius, halo)

    halo = two_halo_ym(one_halo_radius) 
    proj_radius, proj_profile_two_ym, two_theta_ym, two_profile_ym = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 
    proj_radius, two_ym_proj_profile = sz.project_profile(one_halo_radius, halo)

    halo = one_halo(one_halo_radius) + two_halo_ym(one_halo_radius)
    proj_radius, proj_profile_y, y_theta, y_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 
    proj_radius, y_proj_profile = sz.project_profile(one_halo_radius, halo)

    plt.clf(); ax=plt.subplot(1,1,1);
    # sdss.txt
    ax.set_yscale("log"); plt.ylim(1e-8,1e-5); plt.xlim(1e-1,100)
    # sim.txt
    #ax.set_yscale("log"); plt.ylim(1e-8,1e-4); plt.xlim(1e-1,100)
#    ax.set_yscale("linear"); plt.ylim(1e-10,.15e-5)
    ax.set_xscale("log"); 
    plt.plot(y_theta,y_profile,c="red");
    plt.plot(one_theta,one_profile,c="orange")
    plt.plot(two_theta,two_profile,c="green")
    plt.plot(two_theta_ym,two_profile_ym,c="blue")
    meanr,meany,meanyerr = np.genfromtxt(data, unpack=True, skip_header=1)
    ix = meanr < 21
    plt.scatter(meanr[ix],meany[ix],c="cyan");
    plt.errorbar(meanr[ix],meany[ix], yerr=meanyerr[ix],c="cyan");
    plt.xlabel("log radius (arcmin)");plt.ylabel("log Y");
    #meanr,meany,meanyerr = np.genfromtxt("sim.txt", unpack=True, skip_header=1)
    #ix = meanr < 50
    #plt.scatter(meanr[ix],meany[ix])
    #plt.errorbar(meanr[ix],meany[ix], yerr=meanyerr[ix])
    print one_profile[0:10].mean(), two_profile[0:10].mean(), two_profile_ym[0:10].mean(), y_profile[0:10].mean()

    data = np.array([one_theta, proj_radius, one_proj_profile, two_ym_proj_profile, y_proj_profile,
        one_profile, two_profile_ym, y_profile]).T
    np.savetxt("tsz_mass_z.txt", data, "%.6e %.6e %.6e %.6e %.6e %.6e %.6e %.6e", header= \
        "R(arcmin) R(Comovin-Mpc) 1halo 2halo Total Smoothed-1halo Smoothed-2halo Smoothed-Total")
    return proj_radius, one_theta, one_profile, two_theta, two_profile, \
        two_theta_ym, two_profile_ym, y_theta, y_profile

# proj_radius, one_profile, one_proj_profile, two_ym_proj_profile, y_proj_profile,
#     one_profile, two_profile_ym, y_profile
# R(arcmin) R(Comovin-Mpc) 1halo 2halo Total Smoothed-1halo Smoothed-2halo Smoothed-Total
#1.170956e-01 1.000000e-02 3.705252e-06 9.346521e-09 3.714599e-06 1.278707e-06 8.934663e-09 1.287642e-06
# yproj_14.0_0.1.txt



def y(z1,z2,r_samples, corr,  phys_r_samples, h1mwp, data="sdss.txt") :
    import sz_profile
    import configparser

    cosmosis_dir = "output_cross/"
    z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
    da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
    hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5 
    config = configparser.ConfigParser()
    config.read('y_cross_rm_values.ini')
    cosmo = "cosmological_parameters"
    psf_fwhm   = config.getfloat(cosmo, "PSF")
    zed1 = z1
    zed2 =  z2
    zed = (zed1+zed2)/2.

    ix = r_samples <= 20.
    one_halo_radius = r_samples[ix]
    two_halo_radius = r_samples[ix]
    two_halo_ym = corr[ix]

    # the corr function and rad profile have slightly different central amplitudes
    # try replacing corr with h1mpw to see  (phys_r_samples, h1mpw)
    one_halo_y = interp1d(phys_r_samples, h1mwp, \
        bounds_error=False, fill_value="extrapolate")(r_samples[ix])
    #one_halo_y = interp1d(r_samples, corr, \
    #    bounds_error=False, fill_value="extrapolate")(r_samples[ix])

    one_halo = interp1d(one_halo_radius, one_halo_y, bounds_error=False, fill_value="extrapolate")
    two_halo_ym = interp1d(two_halo_radius, two_halo_ym, bounds_error=False, fill_value="extrapolate")

    #==================================
    #
    # main computation
    #
    #==================================
    
    m200 = 1 ;# this won't be used in the project_and_convolve_with_beam
    sz = sz_profile.szProfile(zed,m200,z_table, da_table, hz_table)

    halo = one_halo(one_halo_radius) 
    ix = halo <= 0
    halo[ix] = halo[np.invert(ix)].min()
    proj_radius, proj_profile_one, one_theta, one_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 
    proj_radius, one_proj_profile = sz.project_profile(one_halo_radius, halo)

    halo = two_halo_ym(one_halo_radius) 
    proj_radius, proj_profile_two_ym, two_theta_ym, two_profile_ym = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 
    proj_radius, two_ym_proj_profile = sz.project_profile(one_halo_radius, halo)

    halo = one_halo(one_halo_radius) + two_halo_ym(one_halo_radius)
    proj_radius, proj_profile_y, y_theta, y_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo, psf_fwhm, zed, z_table, da_table) 
    proj_radius, y_proj_profile = sz.project_profile(one_halo_radius, halo)

    plt.clf(); ax=plt.subplot(1,1,1);
    # sdss.txt
    ax.set_yscale("log"); plt.ylim(1e-8,1e-5); plt.xlim(1e-1,100)
    # sim.txt
    #ax.set_yscale("log"); plt.ylim(1e-8,1e-4); plt.xlim(1e-1,100)
#    ax.set_yscale("linear"); plt.ylim(1e-10,.15e-5)
    ax.set_xscale("log"); 
    plt.plot(y_theta,y_profile,c="red");
    plt.plot(one_theta,one_profile,c="orange")
    print one_profile[10]
    print two_profile_ym[10]
    plt.plot(two_theta_ym,two_profile_ym,c="blue")
    meanr,meany,meanyerr = np.genfromtxt(data, unpack=True, skip_header=1)
    ix = meanr < 21
    plt.scatter(meanr[ix],meany[ix],c="green");
    plt.errorbar(meanr[ix],meany[ix], yerr=meanyerr[ix],c="green");
    plt.xlabel("log radius (arcmin)");plt.ylabel("log Y");
    #meanr,meany,meanyerr = np.genfromtxt("sim.txt", unpack=True, skip_header=1)
    #ix = meanr < 50
    #plt.scatter(meanr[ix],meany[ix])
    #plt.errorbar(meanr[ix],meany[ix], yerr=meanyerr[ix])

    #data = np.array([one_theta, proj_radius, one_proj_profile, two_ym_proj_profile, y_proj_profile,
        #one_profile, two_profile_ym, y_profile]).T
    #np.savetxt("tsz_mass_z.txt", data, "%.6e %.6e %.6e %.6e %.6e %.6e %.6e %.6e", header= \
        #"R(arcmin) R(Comovin-Mpc) 1halo 2halo Total Smoothed-1halo Smoothed-2halo Smoothed-Total")
    #return proj_radius, one_theta, one_profile, two_theta, two_profile, \
        #two_theta_ym, two_profile_ym, y_theta, y_profile

#    return proj_radius, one_theta, one_profile, two_theta, two_profile, \
#        two_theta_ym, two_profile_ym, y_theta, y_profile

# proj_radius, one_profile, one_proj_profile, two_ym_proj_profile, y_proj_profile,
#     one_profile, two_profile_ym, y_profile
# R(arcmin) R(Comovin-Mpc) 1halo 2halo Total Smoothed-1halo Smoothed-2halo Smoothed-Total
#1.170956e-01 1.000000e-02 3.705252e-06 9.346521e-09 3.714599e-06 1.278707e-06 8.934663e-09 1.287642e-06
# yproj_14.0_0.1.txt


