import os
import numpy as np
import matplotlib.pyplot as plt

import sz_profile


def do(zed, m200) :
    import configparser
    cosmosis_dir = "output_cross/"

    #==================================
    #
    # cosmological parameters
    #
    #==================================
    config = configparser.ConfigParser()
    config.read('y_cross_rm_values.ini')
    cosmo = "cosmological_parameters"
    omega_b = config.getfloat(cosmo,"omega_b")
    omega_m = config.getfloat(cosmo,"omega_m")

    z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
    da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
    hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5 ;# km/s/Mpc

    #==================================
    #
    # sz profile parameters
    #
    #==================================


    try :
        Ap = config.getfloat(cosmo,"Ap")
    except :
        Ap = config.get(cosmo,"Ap"); 
        Ap = float(Ap.split()[1])
    Bp = config.getfloat(cosmo,"Bp")
    Cp = config.getfloat(cosmo,"Cp")
    Ax = config.getfloat(cosmo,"Ax")
    Bx = config.getfloat(cosmo,"Bx")
    Cx = config.getfloat(cosmo,"Cx")
    try :
        Abeta = config.getfloat(cosmo,"Abeta")
    except :
        Abeta = config.get(cosmo,"Abeta")
        Abeta = float(Abeta.split()[1])
    Bbeta = config.getfloat(cosmo,"Bbeta")
    Cbeta = config.getfloat(cosmo,"Cbeta")
    psf_fwhm   = config.getfloat(cosmo, "PSF")

    #==================================
    #
    # main computation
    #
    #==================================

    sz1 = sz_profile.szProfile(zed,m200,z_table, da_table, hz_table, omega_b, omega_m,
        Ap=Ap, Bp=Bp, Cp=Cp, Ax=Ax, Bx=Bx, Cx=Cx,
        Abeta=Abeta, Bbeta=Bbeta, Cbeta=Cbeta)
    one_halo_radius = sz1.radial
    one_halo= sz1.phys_profile

    proj_radius, proj_profile, one_theta, one_profile = sz1.project_and_convolve_with_beam(
        one_halo_radius, one_halo, psf_fwhm, zed, z_table, da_table)

    plt.clf();ax=plt.subplot(1,1,1);
    ax.set_yscale("log");ax.set_xscale("log");
    plt.plot(one_theta,one_profile);
    plt.xlabel("radius log(arcmin)")
    plt.ylabel("log(Y)")
    plt.title("tSZ profile, psf={:.1f} arcmin".format(psf_fwhm))

    return one_halo_radius, one_halo, one_theta, one_profile

