import os
from cosmosis.datablock import option_section, names
import numpy as np
from scipy.interpolate import interp1d
import sz_profile

def setup(options):
     cons = 'test'
     return cons

def execute(block, config):
    """
    Convention:
        mass is in units of M_sun
        m200 is in units of 10^14 M_sun
    """
    cons = config
    szb = "tSZ"

    #==================================
    #
    # cosmological parameters
    #
    #==================================
    cosmo = names.cosmological_parameters
    doCosmo = block[cosmo,"doCosmo"]

    if doCosmo:
        dis = names.distances
        z_table = block[dis,'z']
        da_table = block[dis,'d_a']
        hz_table = block[dis,'h']*3e5  ;# to get into km/s/Mpc
    else :
        cosmosis_dir = "output_cross/"
        z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
        da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
        hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5

    #==================================
    #
    # sample parameters
    #
    #==================================
    psf   = block[cosmo, "PSF"]
    zed1 = block[cosmo,"z_low"]
    zed2 =  block[cosmo,"z_hi"]
    zed = (zed1+zed2)/2.
    # profile 
    psf_fwhm   = block[cosmo, "PSF"]

    #==================================
    #
    # 3-d profiles
    #
    #==================================
    one_halo_radius = block[szb,'phys_radius_one']
    one_halo_y = block[szb,'profile_one'] 
    two_halo_radius = block[szb,'phys_radius_two']
    two_halo_y = block[szb,'profile_two'] 

    one_halo = interp1d(one_halo_radius, one_halo_y, bounds_error=False, fill_value="extrapolate")
    two_halo = interp1d(two_halo_radius, two_halo_y, bounds_error=False, fill_value="extrapolate")

    #==================================
    #
    # main computation
    #
    #==================================
    
    m200 = 1 ;# this won't be used in the project_and_convolve_with_beam
    sz = sz_profile.szProfile(zed,m200,z_table, da_table, hz_table)

    halo_y = one_halo(one_halo_radius) + two_halo(one_halo_radius)
    y_proj_radius, y_proj_profile, y_theta, y_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo_y, psf_fwhm, zed, z_table, da_table) 

    halo_y = one_halo(one_halo_radius) 
    proj_radius, y_proj_one, one_theta, one_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo_y, psf_fwhm, zed, z_table, da_table) 

    halo_y = two_halo(one_halo_radius) 
    proj_radius, y_proj_two, two_theta, two_profile = sz.project_and_convolve_with_beam(
        one_halo_radius, halo_y, psf_fwhm, zed, z_table, da_table) 


    #==================================
    #
    # Done. Output
    #
    #==================================
    block[szb,'y_theta'] = y_theta
    block[szb,'y_profile'] = y_profile
    block[szb,'y_one_theta'] = one_theta
    block[szb,'y_one_profile'] = one_profile
    block[szb,'y_two_theta'] = two_theta
    block[szb,'y_two_profile'] = two_profile

