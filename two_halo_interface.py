import os
import numpy as np
from cosmosis.datablock import option_section, names
import two_halo

def setup(options):
     cons = 'test'
     return cons

def execute(block, config):
    """
    Two halo term

    Convention:
        mass is in units of M_sun
        m200 is in units of 10^14 M_sun
    """
    cons = config

    #==================================
    #
    # cosmological parameters
    #
    #==================================
    cosmo = names.cosmological_parameters
    doCosmo = block[cosmo,"doCosmo"]
    omega_b = block[cosmo,"omega_b"]
    omega_m = block[cosmo,"omega_m"]
    h0 = block[cosmo,"h0"]

    if doCosmo:
        # matter-matter power spectrum
        mps = "matter_power_lin"
        k_h = block[mps, "k_h"]
        pk_z_table = block[mps, "z"]
        p_k_table=block[mps,"p_k"].reshape([np.size(pk_z_table),np.size(k_h)]).T

    else:
        cosmosis_dir = "output_cross/"
        # matter-matter power spectrum
        pk_z_table = np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/z.txt"), skip_header=1)
        k_h = np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/k_h.txt"), skip_header=1)
        p_k_table=np.genfromtxt(os.path.join(cosmosis_dir,"matter_power_lin/p_k.txt"), \
            skip_header=1).reshape([np.size(pk_z_table),np.size(k_h)]).T

    #==================================
    #
    # sample parameters
    #
    #==================================
    zed1 = block[cosmo,"z_low"]
    zed2 = block[cosmo,"z_hi"]

    #==================================
    #
    # one-halo results
    #
    #=================================
    szb = "tSZ"
    if doCosmo:
    #    phys_r_samples = block[szb,'phys_radius_two_sum'] 
    #    h2_mass_weighted_profile = block[szb,'profile_two_sum'] 
    #    h1_mass_weighted_profile = block[szb,'profile_one'] 
        #  bias in a lambda bin to multiply the matter-matter power spectrum by
        b_mu_bin = block[szb,'b_mu_bin'] 
    else :
    #    phys_r_samples = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/phys_radius_two_sum.txt"), skip_header=1)
    #    h2_mass_weighted_profile = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/profile_two_sum.txt"), skip_header=1)
    #    h1_mass_weighted_profile = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/profile_one.txt"), skip_header=1)

        #  bias in a lambda bin to multiply the matter-matter power spectrum by
        keyword = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/values.txt"), unpack=True,usecols=0,dtype="str")
        values = np.genfromtxt(os.path.join(cosmosis_dir,"tsz/values.txt"), unpack=True,usecols=2)
        ix = keyword == "b_mu_bin"
        b_mu_bin = values[ix]

    #===============================================
    #
    # one-halo parameters needed for two-halo term
    #
    #==============================================

    if doCosmo:
        dis = names.distances
        z_table = block[dis,'z']
        da_table = block[dis,'d_a']
        hz_table = block[dis,'h']*3e5  ;# to get into km/s/Mpc

        # tinker mass function  in critical; to get background mult by omega_m
        mf = names.mass_function
        mz_table=block[mf,"z"]
        m_table=block[mf,"m_h"]
        dndlnMh_table=block[mf,"dndlnMh"].reshape([np.size(mz_table),np.size(m_table)]).T

        # tinker bias
        tbf = "tinker_bias_function"
        tinker_bias_ln_mass = block[tbf,"ln_mass"]
        tinker_bias_bias = block[tbf,"bias"]
        tinker_bias_zed = block[tbf,"z"]

    else:
        cosmosis_dir = "output_cross/"
        z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
        da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
        hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5

        mz_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/z.txt"), skip_header=1)
        m_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/m_h.txt"), skip_header=1)
        dndlnMh_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/dndlnmh.txt"), \
            skip_header=1).reshape([np.size(mz_table),np.size(m_table)]).T

        # tinker bias
        tinker_bias_ln_mass = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/ln_mass.txt"), skip_header=1)
        tinker_bias_zed = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/z.txt"), skip_header=1)
        tinker_bias_bias = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/bias.txt"), skip_header=1)

    #==================================
    #
    # sz profile parameters
    #
    #==================================
    cosmo = names.cosmological_parameters
    Ap = block[cosmo,"Ap"]
    Bp = block[cosmo,"Bp"]
    Cp = block[cosmo,"Cp"]
    Ax = block[cosmo,"Ax"]
    Bx = block[cosmo,"Bx"]
    Cx = block[cosmo,"Cx"]
    Abeta = block[cosmo,"Abeta"]
    Bbeta = block[cosmo,"Bbeta"]
    Cbeta = block[cosmo,"Cbeta"]
    # and we will use Mechior et al 2016 eq 51 and table 4
    m200_pivot =  block[cosmo,"m200_pivot"] # in units of 1e14 M_sun
    Fm =  block[cosmo,"Fm"] # the lambda power law
    Cm =  block[cosmo,"Cm"] # the redshift power law
    sigma =  block[cosmo,"m_sigma"] # mass scatter



    #==================================
    #
    # main computation
    #
    #=================================

    #r_samples, corr, krange, p_k_sz = two_halo.main( \
    #    zed1, zed2, phys_r_samples, 
    #    h1_mass_weighted_profile, h2_mass_weighted_profile, b_mu_bin,
    #    k_h, pk_z_table, p_k_table) 

    r_samples, corr = two_halo.main2 ( zed1, zed2, b_mu_bin,
            k_h, pk_z_table, p_k_table,
            dndlnMh_table, m_table, mz_table,
            z_table, hz_table,
            tinker_bias_zed, tinker_bias_ln_mass, tinker_bias_bias,
            h0, omega_b, omega_m  ) 


    #==================================
    #
    # Done. Output
    #
    #=================================
    block[szb,'phys_radius_two'] = r_samples
    block[szb,'profile_two'] = corr
    #block[szb,'k'] = krange
    #block[szb,'pk'] = p_k_sz

def cleanup(config):
    return 0

