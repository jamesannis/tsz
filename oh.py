import os
import numpy as np
from scipy.interpolate import interp1d
from scipy.interpolate import RectBivariateSpline
import matplotlib.pyplot as plt

import scaling_relations
import cluster_average
import sz_profile
import one_halo


def do(lambda_low,lambda_high,zed1,zed2, data="sdss.txt",plot=True):
    import configparser
    cosmosis_dir = "output_cross/"

    #==================================
    #
    # cosmological parameters
    #
    #==================================
    config = configparser.ConfigParser()
    config.read('y_cross_rm_values.ini')
    cosmo = "cosmological_parameters"
    omega_b = config.getfloat(cosmo,"omega_b")
    omega_m = config.getfloat(cosmo,"omega_m")

    z_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/z.txt"), skip_header=1)
    da_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/d_a.txt"), skip_header=1)
    hz_table = np.genfromtxt(os.path.join(cosmosis_dir,"distances/h.txt"), skip_header=1)*3e5 

    #==================================
    #
    # sz profile parameters
    #
    #==================================

    try :
        Ap = config.getfloat(cosmo,"Ap")
    except :
        Ap = config.get(cosmo,"Ap"); 
        Ap = float(Ap.split()[1])
    Bp = config.getfloat(cosmo,"Bp")
    Cp = config.getfloat(cosmo,"Cp")
    Ax = config.getfloat(cosmo,"Ax")
    Bx = config.getfloat(cosmo,"Bx")
    Cx = config.getfloat(cosmo,"Cx")
    try :
        Abeta = config.getfloat(cosmo,"Abeta")
    except :
        Abeta = config.get(cosmo,"Abeta")
        Abeta = float(Abeta.split()[1])
    Bbeta = config.getfloat(cosmo,"Bbeta")
    Cbeta = config.getfloat(cosmo,"Cbeta")
    # and we will use Mechior et al 2016 eq 51 and table 4
    m200_pivot =  config.getfloat(cosmo,"m200_pivot") # in units of 1e14 M_sun
    Fm =  config.getfloat(cosmo,"Fm") # the lambda power law
    Cm =  config.getfloat(cosmo,"Cm") # the redshift power law
    sigma =  config.getfloat(cosmo,"m_sigma") # mass scatter
    # profile 
    psf_fwhm   = config.getfloat(cosmo, "PSF")

    #==================================
    #
    # sample parameters
    #
    #==================================

    fraction_of_sky = config.getfloat(cosmo,"fraction_of_sky")
    zed = (zed1+zed2)/2.

    # lambda bin limits
    m200_obs_low = scaling_relations.mass_from_lambda (m200_pivot, Fm, Cm, zed, lambda_low) 
    m200_obs_hi  = scaling_relations.mass_from_lambda (m200_pivot, Fm, Cm, zed, lambda_high) 

    # reasonable mass integration limits
    limit_low = np.log(1e12)
    limit_hi =  np.log(1e16)


    # tinker mass function  in critical; to get background mult by omega_m
    mz_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/z.txt"), skip_header=1)
    m_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/m_h.txt"), skip_header=1)
    dndlnMh_table=np.genfromtxt(os.path.join(cosmosis_dir,"mass_function/dndlnmh.txt"), skip_header=1).reshape([np.size(mz_table),np.size(m_table)]).T
    # build mass function interpolator
    area = 4.0*np.pi*fraction_of_sky  # fraction of the sky covered, taking mass function as per sterdadian
    #2D interpolator into mass function; mass in linear in solar masses
    mass_function = RectBivariateSpline(m_table,mz_table,dndlnMh_table*area)

    # tinker bias
    tinker_bias_ln_mass = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/ln_mass.txt"), skip_header=1)
    tinker_bias_zed = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/z.txt"), skip_header=1)
    tinker_bias_bias = np.genfromtxt(os.path.join(cosmosis_dir,"tinker_bias_function/bias.txt"), skip_header=1)
    bias_interpolator_2d = RectBivariateSpline(\
         tinker_bias_zed, tinker_bias_ln_mass, tinker_bias_bias)
    bias = bias_interpolator_2d(zed,tinker_bias_ln_mass)
    bias_interpolator = interp1d(tinker_bias_ln_mass, bias, \
        bounds_error=False, fill_value="extrapolate")

    #==================================
    #
    # main computation
    #
    #==================================

    r_samples, h1_mass_weighted_profile, \
        h2_mass_weighted_profile, b_mu_bin = one_halo.main(
            zed1, zed2, fraction_of_sky, lambda_low, lambda_high,
            z_table, da_table, hz_table, omega_b, omega_m,
            dndlnMh_table, m_table, mz_table,
            tinker_bias_ln_mass, tinker_bias_zed, tinker_bias_bias,
            m200_pivot, Fm, Cm, sigma,
            Ap, Bp, Cp, Ax, Bx, Cx, Abeta, Bbeta, Cbeta)

    # we need a mass averager object, which does the integrals for us
    mass_averager = cluster_average.expectation(zed, mass_function)

    mass, pressures = one_halo.build_press_interp(limit_low, limit_hi, \
        zed, Ap, Bp, Cp, z_table, da_table, hz_table, omega_b, omega_m) 


    print "bias", mass_averager.value_interp( bias_interpolator), \
        b_mu_bin
    print "mass", np.log10(np.exp(mass_averager.value_interp( interp1d(mass, mass )))), \
        np.log10(np.exp( mass_averager.value_interp_lambda_bin( 
        interp1d(mass, mass), m200_obs_low, m200_obs_hi, sigma)))
    print "pressure", mass_averager.value_interp( interp1d(mass, pressures )), \
        mass_averager.value_interp_lambda_bin( 
        interp1d(mass, pressures), m200_obs_low, m200_obs_hi, sigma)
    print "psf (arcmin)",psf_fwhm 

    m200 = 1 ;# this won't be used in the project_and_convolve_with_beam
    sz = sz_profile.szProfile(zed,m200,z_table, da_table, hz_table)
    proj_radius, proj_profile, one_theta, one_profile = sz.project_and_convolve_with_beam(
        r_samples, h1_mass_weighted_profile, psf_fwhm, zed, z_table, da_table)

    if plot :
        plt.clf();ax=plt.subplot(1,1,1);
        ax.set_yscale("log");ax.set_xscale("log");
        plt.plot(one_theta[1:],one_profile[1:]);
        meanr,meany,meanyerr = np.genfromtxt(data, unpack=True, skip_header=1)
        ix = meanr < 21
        #meanr,meany,meanyerr = np.genfromtxt("sim.txt", unpack=True, skip_header=2)
        #ix = meanr < 40
        plt.scatter(meanr[ix],meany[ix],c="cyan");

    cosmosis_dir = "output_cross/"
    np.savetxt(os.path.join(cosmosis_dir,"tsz/phys_radius_one.txt"), r_samples.T, "%e", header="# phys_radius_one")
    np.savetxt(os.path.join(cosmosis_dir,"tsz/profile_one.txt"), h1_mass_weighted_profile.T, "%e", header="# profile_one")
    np.savetxt(os.path.join(cosmosis_dir,"tsz/phys_radius_two_sum.txt"), r_samples.T, "%e", header="# phys_radius_two_sum")
    np.savetxt(os.path.join(cosmosis_dir,"tsz/profile_two_sum.txt"), h2_mass_weighted_profile.T, "%e", header="# profile_two_sum")
    fd=open(os.path.join(cosmosis_dir,"tsz/values.txt"),"w");
    fd.write("b_mu_bin = {}\n".format(b_mu_bin)); fd.close()
    print "b_mu_bin=", b_mu_bin
    
    return r_samples, h1_mass_weighted_profile,  \
        b_mu_bin , one_theta, one_profile, proj_radius, proj_profile


